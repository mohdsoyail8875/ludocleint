import React, { Component, useEffect, useState } from 'react';
import { Router, Switch, Route, useHistory, Redirect } from 'react-router-dom';
import Login from '../app/user-pages/Login';
import Home from './Components/Home';
import Help from './uiComponents/Help';
import Homepage from './uiComponents/Homepage';
import Landing from './uiComponents/Landing';
import userLogin from './uiComponents/Login';
import Profile from './uiComponents/Profile';
import Register from './uiComponents/Register';
import Mywallet from './uiComponents/Mywallet';
import Addcase from './uiComponents/Addcase';
import Addfunds from './uiComponents/Addfunds';
import Withdrawopt from './uiComponents/Withdrawopt';

import Profile1 from './uiComponents/Profile1';
import ViewGame1 from './uiComponents/ViewGame1';

import Gamehistory from './uiComponents/Gamehistory';
// import Transtion from './uiComponents/Transtion';
import 'animate.css';
import axios from 'axios';
import Deposit from './uiComponents/Deposit';
import ViewGame from './uiComponents/ViewGame';
import Transactionhistory from './uiComponents/Transactionhistory';
import Referralhis from './uiComponents/Referralhis';
import Refer from './uiComponents/Refer';
import Notification from './uiComponents/Notification';
import Support from './uiComponents/Support';
import Pan from './uiComponents/Pan';
import Aadhar from './uiComponents/Aadhar';
import Games from './uiComponents/Games';
import Kyc from './uiComponents/Kyc';
import kyc2 from './uiComponents/Kyc2';
import kyc3 from './uiComponents/kyc3';
import RefundPolicy from './uiComponents/RefundPolicy';
import terms_condition from './uiComponents/terms_condition';
import PrivacyPolicy from './uiComponents/PrivacyPolicy';
import Gamerules from './uiComponents/Gamerules';
import Return from './uiComponents/Return';
import Notify from './uiComponents/Notify';
import Timer from './Components/Timer';

const App2 = () => {


        const access_token = localStorage.getItem("token")
        const [user, setUser] = useState()
        useEffect(() => {
                // const access_token = localStorage.getItem("token")

                const headers = {
                        Authorization: `Bearer ${access_token}`
                }
                axios.get(`http://64.227.186.66:4000/me`, { headers })
                        .then((res) => {
                                setUser(res.data)
                                // console.log(res.data);
                        })
        }, [])

        if (!access_token) {
                return (
                        <Switch>
                                <Route exact path="/login" component={userLogin} />
                                <Route exact path="/" component={Games} />
                                <Route path="/transaction-history" component={Transactionhistory} />
                                <Route path="/Referral-history" component={Referralhis} />
                                <Route path="/landing" component={Landing} />
                                <Route path="/gamehistory" component={Gamehistory} />
                                <Route path="/home" component={Home} />

                                {/* <Route path="/Deposit" component={Deposit} /> */}
                                <Route path="/adminlogin" component={Login} />
                                <Route path="/login" component={userLogin} />
                                <Route path="/register" component={Register} />
                                <Route path="/help" component={Help} />
                                <Route path="/refer" component={Refer} />
                                <Route path="/Games" component={Games} />
                                <Route path="/support" component={Support} />
                                <Route path="/kyc" component={Kyc} />
                                <Redirect to="/login" />
                                <Route path="/RefundPolicy" component={RefundPolicy}/>
                                <Route path="/PrivacyPolicy" component={PrivacyPolicy}/>
                                <Route path="/terms" component={terms_condition}/>
                                <Route path="/Rules" component={Gamerules}/>

                        </Switch >
                )
        } else {
                return (
                        <Switch>
                                <Route exact path="/Homepage" component={Homepage} />
                                <Route path="/transaction-history" component={Transactionhistory} />
                                <Route exact path="/home" component={Home} />
                                <Route exact path="/transaction" component={Transactionhistory} />
                                <Route exact path="/home" component={Home} />
                                <Route exact path="/Referral-history" component={Referralhis} />
                                <Route exact path="/landing" component={Landing} />
                                <Route exact path="/adminlogin" component={Login} />
                                <Route exact path="/Deposit" component={Deposit} />
                                <Route exact path="/login" component={userLogin} />
                                <Route exact path="/register" component={Register} />
                                <Route exact path="/gamehistory" component={Gamehistory} />
                                <Route exact path="/profile" component={Profile} />
                                <Route exact path="/help" component={Help} />
                                <Route exact path="/viewgame/:id" component={ViewGame} />
                                <Route exact path="/HomePage/:Game" component={Homepage} />
                                <Route exact path="/refer" component={Refer} />
                                <Route exact path="/Notification" component={Notification} />
                                <Route exact path="/" component={Games} />
                                <Route path="/profile1" component={Profile1} />
                                <Route path="/viewgame1/:id" component={ViewGame1} />
                                <Route path="/addcase" component={Addcase} />
                                <Route path="/add-funds" component={Addfunds} />
                                <Route path="/withdrawopt" component={Withdrawopt} />
                                <Route path="/wallet" component={Mywallet} />
                                <Route path="/support" component={Support} />
                                <Route path="/KYC/update-pan" component={Pan} />
                                <Route path="/KYC/update-aadhar" component={Aadhar} />
                                <Route path="/Games" component={Games} />
                                <Route exact path="/landing/:id" component={Landing} />
                                <Route path="/kyc" component={Kyc} />
                                <Route path="/kyc2" component={kyc2} />
                                <Route path="/kyc3" component={kyc3} />
                                <Route path="/Rules" component={Gamerules}/>
                                <Route path="/RefundPolicy" component={RefundPolicy}/>
                                <Route path="/PrivacyPolicy" component={PrivacyPolicy}/>
                                <Route path="/terms" component={terms_condition}/>
                                <Route path="/timer" component={Timer}/>
                                <Route path="/return" component={Return} />
                                <Route path="/notify" component={Notify} />

                                {user && user.user_type === "User" && <Redirect to="/Homepage" />}
                        </Switch>
                )
        }
}
export default App2;
