import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Link, useHistory } from 'react-router-dom';

import "../Components/Component-css/Header.css";
import css from "./Component-css/Nav.module.css";
const w3_close = () => {
  const width=document.getElementById("mySidebar").offsetWidth;
  document.getElementById("mySidebar").style.left = `-${width}px`;
  document.getElementById('sidebarOverlay').style.display = 'none';

};
const w3_open = () => {
  document.getElementById("mySidebar").style.left = "0";
  document.getElementById('sidebarOverlay').style.display = 'block';
};

const Header = () => {

  const history = useHistory()
  const [user, setUser] = useState()


  // const access_token = localStorage.getItem("token")

  const logout = () => {
    let access_token = localStorage.getItem("token")
    const headers = {
      Authorization: `Bearer ${access_token}`
    }
    axios.post(`http://192.168.1.55:4000/logout`, {
      headers: headers
    }, { headers })
      .then((res) => {
        // setUser(res.data)
        history.push('/landing')
        localStorage.removeItem("token")
        window.location.reload()
      }).catch((e) => {
        alert(e.msg)
      })

  }


  // const [cash, setCash] = useState()

  const Cashheader = () => {
    let access_token = localStorage.getItem("token")
    const headers = {
      Authorization: `Bearer ${access_token}`
    }
    axios.get(`http://64.227.186.66:4000/me`, { headers })
      .then((res) => {
        setUser(res.data)
        // console.log(res.data);
      }).catch((e) => {
        alert(e.msg)
      })

  }

  useEffect(() => {
    Cashheader()

  }, [])

  if (user == undefined) {
    return null
  }


  return (
    <div>
        <div id='sidebarOverlay' onClick={w3_close}></div>
      {/* Sidebar */}
      <div className="w3-sidebar w3-bar-block" id="mySidebar" style={{paddingBottom:'70px'}}>
        {/* <button onClick={w3_close} className="w3-bar-item w3-button w3-large">
          Close 
        </button> */}
        <Link to="/profile1" className="w3-bar-item w3-button">
        <picture className="icon">
            <img src={process.env.PUBLIC_URL + '/Images/LandingPage_img/Header_profile.png'} width="30px" height="30px" alt="profile" />
          </picture>
          <div style={{ marginLeft: '.5rem' }}>
            My Profile
          </div>
        </Link>
        <Link to ="/Homepage" className="w3-bar-item w3-button">
          <picture className="icon">
            <img src={process.env.PUBLIC_URL + '/Images/Header/gamepad.png'} />
          </picture>
          <div style={{ marginLeft: '.5rem' }}>
            Win cash
          </div>
          {/* <picture className="arrow">
            <img src="https://khelbhai.in/images/global-black-chevronRight.png" />
          </picture> */}
        </Link>
        <Link to="/wallet" className="w3-bar-item w3-button">
          <picture className="icon">
            <img src={process.env.PUBLIC_URL + '/Images/Header/wallet.png'} />
          </picture>
          <div style={{ marginLeft: '.5rem' }}>
            My wallet
          </div>
          {/* <picture className="arrow">
            <img src="https://khelbhai.in/images/global-black-chevronRight.png" />
          </picture > */}
        </Link>

        <Link to="/" className="w3-bar-item w3-button">
          <picture className="icon">
            <img src={process.env.PUBLIC_URL + '/Images/Header/gamesHistory.png'} />
          </picture>
          <div style={{ marginLeft: '.5rem' }}>
            Game history
          </div>
          {/* <picture className="arrow">
            <img src="https://khelbhai.in/images/global-black-chevronRight.png" />
          </picture> */}
        </Link>

        <Link to="/" className="w3-bar-item w3-button">
          <picture className="icon">
            <img src={process.env.PUBLIC_URL + '/Images/Header/order-history.png'} />
          </picture>
          <div style={{ marginLeft: '.5rem' }}>
            Transaction history
          </div>
          {/* <picture className="arrow">
            <img src="https://khelbhai.in/images/global-black-chevronRight.png" />
          </picture> */}
        </Link>

        <Link to="/refer" className="w3-bar-item w3-button">
          <picture className="icon">
            <img src={process.env.PUBLIC_URL + '/Images/Header/sreferEarn.png'} />
          </picture>
          <div style={{ marginLeft: '.5rem' }}>
            Refer and Earn
          </div>
          {/* <picture className="arrow">
            <img src="https://khelbhai.in/images/global-black-chevronRight.png" />
          </picture> */}
        </Link>

        <Link to="/" className="w3-bar-item w3-button">
          <picture className="icon">
            <img src={process.env.PUBLIC_URL + '/Images/Header/refer-history.webp'} />
          </picture>
          <div style={{ marginLeft: '.5rem' }}>
            Refer history
          </div>
          {/* <picture className="arrow">
            <img src="https://khelbhai.in/images/global-black-chevronRight.png" />
          </picture> */}
        </Link>

        <Link to="/Notification" className="w3-bar-item w3-button">
          <picture className="icon">
            <img src={process.env.PUBLIC_URL + '/Images/Header/notifications.png'} />
          </picture>
          <div style={{ marginLeft: '.5rem' }}>
            Notification
          </div>
          {/* <picture className="arrow">
            <img src="https://khelbhai.in/images/global-black-chevronRight.png" />
          </picture> */}
        </Link>

        <Link to="/help" className="w3-bar-item w3-button">
          <picture className="icon">
            <img src={process.env.PUBLIC_URL + '/Images/Header/support.png'} />
          </picture>
          <div style={{ marginLeft: '.5rem' }}>
            Help
          </div>
          {/* <picture className="arrow">
            <img src="https://khelbhai.in/images/global-black-chevronRight.png" />
          </picture> */}
        </Link>

        <Link to="/support" className="w3-bar-item w3-button">
          <picture className="icon">
            <img src={process.env.PUBLIC_URL + '/Images/Header/support.png'} />
          </picture>
          <div style={{ marginLeft: '.5rem' }}>
            Support
          </div>
          {/* <picture className="arrow">
            <img src="https://khelbhai.in/images/global-black-chevronRight.png" />
          </picture> */}
        </Link>
        <Link className="w3-bar-item w3-button" onClick={(e) => logout(e)}>
          <picture className="icon">
            <img src={process.env.PUBLIC_URL + '/Images/Header/support.png'} />
          </picture>
          <div style={{ marginLeft: '.5rem' }}>
             Logout
          </div>

        </Link>
      </div>


      {/* Page Content */}

      <div className="w3-teal">
        <div className="w3-container ">
          <div className={`${css.headerContainer} `}>
            <button
              className="w3-button w3-teal w3-xlarge float-left"
              onClick={w3_open} id="hambergar"
            >
              <picture className={`${css.sideNavIcon} mr-0`}>
                <img
                  src={process.env.PUBLIC_URL + '/Images/LandingPage_img/sidebar.png'}
                  className="snip-img"
                  alt=''
                />
              </picture>
            </button>
            <Link to="/" className>
              <picture className={`ml-2 ${css.navLogo} d-flex`}>
                <img
                  src={process.env.PUBLIC_URL + '/Images/LandingPage_img/Header_profile.png'}
                  className="snip-img"
                  alt='Khelbro'
                />
              </picture>
            </Link>
            <div className>
              <div className={`${css.menu_items}`}>
                <Link className={`${css.box}`} to="/Deposit">
                  <picture className={`${css.moneyIcon_container}`}>
                    <img
                      src={process.env.PUBLIC_URL + "/Images/LandingPage_img/global-rupeeIcon.png"}
                      className="snip-img"
                      alt=''
                    />
                  </picture>
                  <div className="mt-1 ml-1">
                    <div className={`${css.moneyBox_header}`}>Cash</div>
                    <div className={`${css.moneyBox_text}`}>{user.Wallet_balance}</div>
                  </div>
                  <picture className={`${css.moneyBox_add}`}>
                    <img
                      src={process.env.PUBLIC_URL + '/Images/LandingPage_img/addSign.png'}
                      className="snip-img"
                      alt=''
                    />
                  </picture>
                </Link>
                <Link
                  className={`${css.box} ml-2`}
                  to="/redeem/refer"
                  style={{ width: "80px" }}
                >
                  <picture className={`${css.moneyIcon_container}`}>
                    <img
                      src={process.env.PUBLIC_URL + '/Images/LandingPage_img/notification-activity-reward.png'}
                      className="snip-img"
                      alt=''
                    />
                  </picture>
                  <div className="mt-1 ml-1">
                    <div className={`${css.moneyBox_header}`}>Earning</div>
                    <div className={`${css.moneyBox_text}`}>{user.referral_wallet}</div>
                  </div>
                </Link>
              </div>
              <span className="mx-5"></span>
            </div>
            <span className="mx-5"></span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
