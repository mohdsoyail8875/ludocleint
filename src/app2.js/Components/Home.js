import React from "react";
import Header from "./Header";
import Footer from "./Footer";
import Scrollbanner from "./Scrollbanner";
import Walletchips from "./Walletchips";
import "./Component-css/Home.css";
import { Link } from 'react-router-dom';

const Home = () => {
  return (
    <div>
      <div className="leftContainer">
        <Header />
        <Scrollbanner/>
        <Walletchips/>
      <section id="hero" className="d-flex align-items-center">
        <div className="container px-3">
          <div className="row">
            <div className="col-12 pt-2 order-1 d-flex flex-column justify-content-center">
              <h1>
                Play Ludo &amp; Earn Real Money &amp; Withdraw It through PayTM
              </h1>
              <h2>
                If you already have an account, please click login button. Or,
                if you are a new user, click on register button.
              </h2>
              <div>
                <br />
                <Link
                  to="/Homepage"
                  className="btn-get-started scrollto"
                >
                  Go To Dashboard
                </Link>
              </div>
            </div>
            <div className="col-12 hero-img text-center">
              <img
                src="https://ludo5.com/assets/img/banner.png"
                className="img-fluid"
                alt=""
                style={{ maxWidth: "300px" }}
              />
            </div>
          </div>
        </div>
      </section>
          <Footer/>
       </div>
        <div className="rightContainer">Right</div>
    </div>
  );
};

export default Home;
