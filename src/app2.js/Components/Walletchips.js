import axios from 'axios'
import React, { useEffect, useState } from 'react'
import "./Component-css/chips.css"

const Walletchips = () => {

  const [user, setUser] = useState()

  const role = async () => {
    const access_token = localStorage.getItem("token")
    const headers = {
      Authorization: `Bearer ${access_token}`
    }
    await axios.get(`http://64.227.186.66:4000/me`, { headers })
      .then((res) => {
        setUser(res.data)
        // console.log(res.data);
      })

  }
  useEffect(() => {
    role()
  }, [])

  if (user == undefined) {
    return null
  }

  return (
    <div>
      <div className="chips text-center bg-success text-light py-1">
        Wallet Chips: <span id="balance">{user.Wallet_balance}</span>
      </div>

    </div>
  )
}

export default Walletchips