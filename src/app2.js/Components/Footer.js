import React from 'react'
import "../Components/Component-css/Footer.css"
import { Link} from 'react-router-dom';

const Footer = () => {
  return (
    <div>
    <footer className="footer">
  <div className="container">
    <nav className="bottom-nav">
      <div className="bottom-nav-item active">
        <div className="bottom-nav-link">
          <Link to="https://ludo5.com/user/home" className=" text-white ">
            <i className="fas fa-home" /><br />
            <span>Home</span>
          </Link>
        </div>
      </div>
      <div className="bottom-nav-item">
        <div className="bottom-nav-link">
          <Link to="https://ludo5.com/user/mytransactions" className=" text-white ">
            <i className="fas fa-dice-six" /><br />
            <span>Transaction History</span>
          </Link>
        </div>
      </div>
      <div className="bottom-nav-item">
        <div className="bottom-nav-link">
          <Link to="https://ludo5.com/user/deposit" className=" text-white ">
          <i class="fa-solid fa-wallet"></i><br />
            <span>Deposit</span>
          </Link>
        </div>
      </div>
      <div className="bottom-nav-item">
        <div className="bottom-nav-link">
          <Link to="https://ludo5.com/user/withdrawl" className=" text-white ">
            <i className="fas fa-university" /><br />
            <span>Withdraw</span>
          </Link>
        </div>
      </div>
    </nav>
  </div>
</footer>

    </div>
  )
}

export default Footer