import React, { Profiler } from 'react'

const Rightcontainer = () => {
  return (
    <div>
         <div className='rightContainer'>
                <div className="rcBanner flex-center ">
                    <picture className="rcBanner-img-container animate__bounce infinite ">
                        <img src={process.env.PUBLIC_URL + '/Images/LandingPage_img/Header_profile.png'} alt="" />
                    </picture>
                    <div className="rcBanner-text " style={{fontWeight:'bolder'}}>khel bhai <span className="rcBanner-text-bold" style={{fontWeight:'normal'}}>Cash Jeet</span></div>
                    <div className="rcBanner-footer">For best experience, open&nbsp;<a href="/" style={{
                        color: 'rgb(44, 44, 44)',
                        fontWeight: 500, textDecoration: 'none'
                    }}>khelbhai.com</a>&nbsp;on&nbsp;<img src={process.env.Public_URL + '/Images/chrome.png'}
                        alt="" />&nbsp;chrome mobile</div>
                </div>
            </div>
    </div>
  )
}

export default Rightcontainer