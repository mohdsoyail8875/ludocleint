import React from "react";
import css from "./Component-css/Nav.module.css";
import { useState } from "react";
import { Offcanvas } from "bootstrap";

const NavBar = () => {
  const show = () => {
    document.getElementById("mySidebar").style.left = "-200px";

}
const hide = () => {
    document.getElementById("mySidebar").style.left = "0";

}

  return (
    <div>
      <div className={`${css.sidebar}`} id="mySidebar">
        <div className={`${css.sidebar_overlay}`} ></div>
        <div className={`${css.sidebar_content}`}>
          <div className="top-head">
            <div className="name">Mohan Khadka</div>
            <div className="email">contact@mohankhadka.com.np</div>
          </div>
          <div className={`${css.nav_left}`}>
            <a href="#home">
              <span className="ion-ios-home-outline" /> Home
            </a>
            <a href="#alarm">
              <span className="ion-ios-list-outline" /> Alarm
            </a>
            <a href="#compose">
              <span className="ion-ios-compose-outline" /> Compose
            </a>
            <a href="#chats">
              <span className="ion-ios-chatboxes-outline" /> Chats
            </a>
            <a href="#profile">
              <span className="ion-ios-person-outline" /> Profile
            </a>
            <a href="#settings">
              <span className="ion-ios-gear-outline" /> Settings
            </a>
            <a href="#credits">
              <span className="ion-ios-information-outline" /> Credits
            </a>
          </div>
        </div>
      </div>

      <div className={`${css.headerContainer} `}>
        <button
          // href="#!"
          className="cxy h-100"
          onClick={() => {
            show();
          }}
          style={{ border: "none", background: "#fff" }}
        >
          <picture className={`${css.sideNavIcon} ml-3 mr-2`}>
            <img
              src="https://khelbhai.in/images/header-hamburger.png"
              className="snip-img"
            />
          </picture>
        </button>

        <a href="/" className>
          <picture className={`ml-2 ${css.navLogo} d-flex`}>
            <img
              src="https://khelbhai.in/images/khelbro.png"
              className="snip-img"
            />
          </picture>
        </a>
        <div className>
          <div className={`${css.menu_items}`}>
            <a className={`${css.box}`} href="/add-funds">
              <picture className={`${css.moneyIcon_container}`}>
                <img
                  src="https://khelbhai.in/images/global-rupeeIcon.png"
                  className="snip-img"
                />
              </picture>
              <div className="mt-1 ml-1">
                <div className={`${css.moneyBox_header}`}>Cash</div>
                <div className={`${css.moneyBox_text}`}>90</div>
              </div>
              <picture className={`${css.moneyBox_add}`}>
                <img
                  src="https://khelbhai.in/images/global-addSign.png"
                  className="snip-img"
                />
              </picture>
            </a>
            <a
              className={`${css.box} ml-2`}
              href="/redeem/refer"
              style={{ width: "80px" }}
            >
              <picture className={`${css.moneyIcon_container}`}>
                <img
                  src="https://khelbhai.in/images/notification-activity-reward.png"
                  className="snip-img"
                />
              </picture>
              <div className="mt-1 ml-1">
                <div className={`${css.moneyBox_header}`}>Earning</div>
                <div className={`${css.moneyBox_text}`}>132</div>
              </div>
            </a>
          </div>
          <span className="mx-5"></span>
        </div>
        <span className="mx-5"></span>
      </div>
    </div>
  );
};

export default NavBar;
