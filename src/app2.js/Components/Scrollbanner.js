import React from 'react'

const Scrollbanner = () => {
  return (
    <div>
      <div className="bg-warning text-danger scroller pt-1">
        <marquee behavior="scroll" direction="left">
          <p><b>Invite friends and you get 1% of their every winnings</b></p><p><span style={{ fontSize: '1rem' }}><b>दोस्तों को आमंत्रित करें और आपको उनकी हर जीत का 1% मिलता है</b></span></p>
        </marquee>
      </div>
    </div>
  )
}

export default Scrollbanner