import React, { Component } from 'react'


// const URL = 'wss://cfrc-v5-services.ludokingapi.com/v5/socket.io/?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MjVkMTEyMTRlNTZiYTM5MWU2N2YyNmYiLCJlVXNlclR5cGUiOjIsImlhdCI6MTY1MjM0OTg5OX0.CM8L5QyDXqtKQmIpKD4R3sQWwjpwtWiw6cZ6FV-qVNI&bConnectAfterLogin=1&nVersion=5.2&nDevice=5&nVersionCode=171&pkgid=com.ludoking.fb&EIO=3&transport=websocket'

class Chat extends Component {
    state = {
        name: 'Bob',
        messages: [],
    }

    // ws = new WebSocket(URL)

   

    addMessage = message =>
        this.setState(state => ({ messages: [message, ...state.messages] }))

    submitMessage = messageString => {
        // on submitting the ChatInput form, send the message, add it to the list and reset the input
        const message = { name: this.state.name, message: messageString }
        this.ws.send(`420["LUDO_private_room",{"gameType":0,"nChipsToAdd":0,"nGemsToAdd":0,"iMaxPlayer":4,"iLobbyChips":100,"iLobbyWinAmount":300,"iLobbyId":1,"_userId":"625d11214e56ba391e67f26f","bUndo":0,"vPrivateCode":"newTable","bCheckOtherTable":false}]`)
        this.addMessage(message)
    }

    render() {
        return (
            <div>
                {/* <label htmlFor="name">
                    Name:&nbsp;
                    <input
                        type="text"
                        id={'name'}
                        placeholder={'Enter your name...'}
                        value={this.state.name}
                        onChange={e => this.setState({ name: e.target.value })}
                    />
                </label> */}
                <input
                    type="submit"
                    // ws={this.ws}
                    onClick={messageString => this.submitMessage(messageString)}
                />

            </div >
        )
    }
}

export default Chat