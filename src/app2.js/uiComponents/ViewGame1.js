import React, { useEffect, useState } from 'react'
import '../css/viewGame1.css'
import "../css/layout.css"
import axios from 'axios';
import { useHistory, useLocation } from 'react-router-dom';
import Header from '../Components/Header';
import socketIOClient from "socket.io-client";
import css from '../css/Pan.module.css'
import Rightcontainer from '../Components/Rightcontainer';
import Swal from 'sweetalert2';
import Countdown from 'react-countdown';
import '../css/Loader.css';
import Timer from '../Components/Timer';


export default function ViewGame1() {
  const history = useHistory()

  const socket = socketIOClient("http://64.227.186.66:4000");

  const location = useLocation();
  const path = location.pathname.split("/")[2];

  const [Game, setGame] = useState()
  const [status, setStatus] = useState(null);
  const [fecthStatus, setFecthStatus] = useState()
  const [scrnshot, setScrnshot] = useState(null)
  const [reason, setReason] = useState(null)

  const randomcode = () => {
    const code = Math.floor((Math.random() * 1000000) + 1)
    return code
  }

  const getPost = async () => {
    const access_token = localStorage.getItem("token")
    const headers = {
      Authorization: `Bearer ${access_token}`
    }
    await axios.patch(`http://64.227.186.66:4000/challange/roomcode/${path}`,
      {
        // Room_code: randomcode(),
        Status: "running"
      }
      , { headers })
      .then((res) => {
        setGame(res.data)
        socket.emit('challengeOngoing', res.data);
      })
      .catch((e) => alert(e))
  }
  /// user details start


  const [user, setUser] = useState()

  const role = async () => {
    const access_token = localStorage.getItem("token")
    const headers = {
      Authorization: `Bearer ${access_token}`
    }
    await axios.get(`http://64.227.186.66:4000/me`, { headers })
      .then((res) => {
        setUser(res.data._id)
        // console.log(res.data._id)
        Allgames(res.data._id)

      })

  }

  /// user details end

  const [ALL, setALL] = useState()

  const Allgames = async (userId) => {
    const access_token = localStorage.getItem('token')
    const headers = {
      Authorization: `Bearer ${access_token}`
    }

    await axios.get(`http://64.227.186.66:4000/challange/${path}`, { headers })
      .then((res) => {
        setALL(res.data)
        setGame(res.data)
        // console.log(res.data.Accepetd_By._id)
        if (userId == res.data.Accepetd_By._id)
          setFecthStatus(res.data.Acceptor_status)

        if (userId == res.data.Created_by._id)
          setFecthStatus(res.data.Creator_Status)

        // if (userId == res.data.Created_by._id && res.data.Status == 'requested' && res.data.Status !== 'ongoing')
        // getPost()
      })
  }

  useEffect(() => {

    role();
    Allgames();
  }, [])


  // Result


  const Result = async () => {
    const access_token = localStorage.getItem("token")
    // console.log(access_token)
    const headers = {
      Authorization: `Bearer ${access_token}`
    }
    if (status) {
      const formData = new FormData();
      formData.append('file', scrnshot);
      formData.append('status', status);
      if (status == 'cancelled'){
        formData.append('reason', reason);
      }
      console.log(formData)
      await fetch(`http://64.227.186.66:4000/challange/result/${path}`, {
        method: "POST",
        body: formData,
        headers
      }).then((res) => {
        res.json().then((result) => {
          history.push("/Games")
          console.warn(result);
        })

      })
    }
    else {
      alert('please fill all field')
    }
    // await axios.patch(`http://64.227.186.66:4000/challange/result/${path}`,
    //   {
    //     status: status
    //   },
    //   { headers })
    //   .then((res) => {
    //     // getPost(res.data)
    //     console.log(res.data);
    //     history.push("/landing")
      // }).catch((e) => {
    //     console.log(e);
    //   })
  }



  const copyCode = (e) => {
    console.log(Game.Room_code);
    navigator.clipboard.writeText(Game.Room_code);
    
    Swal.fire({
      position: 'center',
      icon: 'success',
      type: 'success',
      title: 'Room Code Copied',
      showConfirmButton: false,
      timer: 1200,
      // width: '50px',
      // height: '50px',
      
    });
   
  }
  const Completionist = () => <span>You are good to go!</span>;
  return (
    <>
      {!Game && <div class="lds-ripple"><div></div><div></div></div>}
      {Game && <div className='leftContainer'>

        <Header />
        <div className='main-area' style={{ paddingTop: '60px' }}>
          <div className='battleCard-bg'>
            <div className='battleCard'>
              <div className='players cxy pt-2'>
                <div className='flex-column cxy'>
                  {Game.Created_by.Name}
                  <img
                    src={process.env.PUBLIC_URL + '/Images/Homepage/Avatar4.png'}
                    width='40px'
                    alt=''
                  />
                </div>
                <img
                  className='mx-3'
                  src={process.env.PUBLIC_URL + '/Images/Homepage/versus.png'}
                  width='23px'
                  alt=''
                />
                <div className='flex-column cxy'>
                  {Game.Accepetd_By.Name}
                  <img src={process.env.PUBLIC_URL + '/Images/Homepage/Avatar4.png'} width='40px' alt='' />
                </div>
              </div>
              <div className='amount cxy mt-2'>
                Playing for
                <img
                  className='mx-1'
                  src={process.env.PUBLIC_URL+'/Images/LandingPage_img/global-rupeeIcon.png'}
                  width='21px'
                  alt=''
                />
                <span style={{ fontSize: '1.2em', fontWeight: 700 }}>{Game.Game_Ammount}</span>
              </div>
              <div className='thin-divider-x my-3' />
              <div ><Timer/></div>
              {Game.Room_code!==null &&
             <div className='roomCode cxy flex-column'>
              
              <div>
                <div>Room Code</div>
                <span>{Game.Room_code}</span>
              </div>
                <button className='bg-green playButton position-static mt-2' onClick={(e) => copyCode(e) } >
                  Copy Code
                </button>
              </div>
              ||Game.Room_code==null &&
              <div className='roomCode cxy flex-column'>
                Waiting for Room Code
                <div className="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                
              </div>

              }
            
              
           
              <div className='cxy app-discription flex-column'>
                Play ludo game in Ludo King App
                <div className='mt-2'>
                  <a
                    href='https://play.google.com/store/apps/details?id=com.ludo.king'
                    target='_blank'
                    rel='noopener noreferrer'
                  >
                    <img
                      className='mr-2'
                      src={process.env.PUBLIC_URL + '/Images/google-play.jpeg'}
                      width='128px'
                      height='38px'
                      alt=''
                    />
                  </a>
                  <a
                    href='https://itunes.apple.com/app/id993090598'
                    target='_blank'
                    rel='noopener noreferrer'
                  >
                    <img
                      src={process.env.PUBLIC_URL + '/Images/app-store.jpeg'} 
                      width='128px'
                      height='38px'
                      alt=''
                    />
                  </a>
                </div>
              </div>
              <div className='thin-divider-x my-3' />
              <div className='rules'>
                <span className='cxy'>
                  <u>Game Rules</u>
                </span>
                <ol className='list-group list-group-numbered'>
                  <li className='list-group-item'>
                    Record every game while playing.
                  </li>
                  <li className='list-group-item'>
                    For cancellation of game, video proof is necessary.
                  </li>
                  <li className='list-group-item'>
                    <img
                      className='mx-1'
                      src={process.env.PUBLIC_URL+'Images/LandingPage_img/global-rupeeIcon.png'}
                      width='21px'
                      alt=''
                    />
                    50 Penality will be charged for updating wrong result.
                  </li>
                  <li className='list-group-item'>
                    <img
                      className='mx-1'
                      src={process.env.PUBLIC_URL+'Images/LandingPage_img/global-rupeeIcon.png'}
                      width='21px'
                      alt=''
                    />
                    25 Penality will be charged for not updating result.
                  </li>
                </ol>
              </div>
              <div className='match-status-border row'>
                <div className='col-6'>Match Status</div>
              </div>
              <form className='result-area' onSubmit={(e) => { e.preventDefault(); Result() }} encType="multipart/form-data">

                {fecthStatus !== null && fecthStatus !== undefined && <p>You have already updated your battle result for <h6 className='d-inline-block text-uppercase'><b>{fecthStatus}</b></h6></p>}
                {fecthStatus === null && <> <p>
                  After completion of your game, select the status of the game
                  and post your screenshot below.
                </p>
                  <div
                    className='MuiFormGroup-root radios'
                    role='radiogroup'
                    aria-label='Result'
                  >
                    <label className='MuiFormControlLabel-root hidden Mui-disabled'>
                      <span
                        className='MuiButtonBase-root MuiIconButton-root jss1 MuiRadio-root MuiRadio-colorSecondary jss2 Mui-checked jss3 Mui-disabled MuiIconButton-colorSecondary Mui-disabled Mui-disabled'
                        tabIndex={-1}
                        aria-disabled='true'
                      >
                        <span className='MuiIconButton-label'>
                          <input
                            className='jss4 mr-2'

                            name='battleResult'
                            type='radio'
                            defaultValue='select'
                            defaultChecked
                            style={{ transform: 'scale(1.5)' }}
                          />
                          {/* <div className='jss5 jss7'>
                          <svg
                            className='MuiSvgIcon-root'
                            focusable='false'
                            viewBox='0 0 24 24'
                            aria-hidden='true'
                          >
                            <path d='M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z' />
                          </svg>
                          <svg
                            className='MuiSvgIcon-root jss6'
                            focusable='false'
                            viewBox='0 0 24 24'
                            aria-hidden='true'
                          >
                            <path d='M8.465 8.465C9.37 7.56 10.62 7 12 7C14.76 7 17 9.24 17 12C17 13.38 16.44 14.63 15.535 15.535C14.63 16.44 13.38 17 12 17C9.24 17 7 14.76 7 12C7 10.62 7.56 9.37 8.465 8.465Z' />
                          </svg>
                        </div> */}
                        </span>
                      </span>
                      <span className='MuiTypography-root MuiFormControlLabel-label Mui-disabled MuiTypography-body1' style={{ marginTop: '3px' }}>
                        (Disabled option)
                      </span>
                    </label>
                    <label className='MuiFormControlLabel-root'>
                      <span
                        className='MuiButtonBase-root MuiIconButton-root jss1 MuiRadio-root jss8'
                        aria-disabled='false'
                      >
                        <span className='MuiIconButton-label'>
                          <input
                            className='jss4 mr-2'
                            name='battleResult'
                            type='radio'
                            defaultValue='winn'
                            onClick={(e) => setStatus("winn")}
                            style={{ transform: 'scale(1.5)' }}
                          />

                        </span>
                        <span className='MuiTouchRipple-root' />
                      </span>
                      <span className='MuiTypography-root MuiFormControlLabel-label MuiTypography-body1' style={{ marginTop: '3px' }}>
                        I Won
                      </span>
                    </label>
                    <label className='MuiFormControlLabel-root'>
                      <span
                        className='MuiButtonBase-root MuiIconButton-root jss1 MuiRadio-root MuiRadio-colorSecondary MuiIconButton-colorSecondary'
                        aria-disabled='false'
                        root='[object Object]'
                      >
                        <span className='MuiIconButton-label'>
                          <input
                            className='jss4 mr-2'
                            name='battleResult'
                            type='radio'
                            defaultValue='lose'
                            onClick={(e) => setStatus("lose")}
                            style={{ transform: 'scale(1.5)' }}
                          />
                        </span>
                        <span className='MuiTouchRipple-root' />
                      </span>
                      <span className='MuiTypography-root MuiFormControlLabel-label MuiTypography-body1' style={{ marginTop: '3px' }}>
                        I Lost
                      </span>
                    </label>
                    <label className='MuiFormControlLabel-root'>
                      <span
                        className='MuiButtonBase-root MuiIconButton-root jss1 MuiRadio-root'
                        aria-disabled='false'
                      >
                        <span className='MuiIconButton-label'>
                          <input
                            className='jss4 mr-2'
                            name='battleResult'
                            type='radio'
                            defaultValue='cancelled'
                            onClick={(e) => setStatus("cancelled")}
                            style={{ transform: 'scale(1.5)' }}

                          />
                        </span>
                        <span className='MuiTouchRipple-root' />
                      </span>
                      <span className='MuiTypography-root MuiFormControlLabel-label MuiTypography-body1' style={{ marginTop: '3px' }}>
                        Cancel
                      </span>
                    </label>
                  </div></>}
                {status !== null && status !== 'cancelled' && status !== 'lose' && <div className={`${css.doc_upload} mt-5`}>
                  <input type="file" onChange={(e) => setScrnshot(e.target.files[0])} required />
                  {!scrnshot && <div className="cxy flex-column position-absolute ">
                    <img src={process.env.PUBLIC_URL + '/Images/upload_icon.png'}  width="17px" alt="" className="snip-img" />
                    <div className={`${css.sideNav_text} mt-2`}>
                      Upload screenshot.
                    </div>
                  </div>}
                  {scrnshot && <div className={css.uploaded}>
                    <img src="https://khelbhai.in/images/file-icon.png" width="26px" alt="" style={{ marginRight: '20px' }} />
                    <div className="d-flex flex-column" style={{ width: '80%' }}>
                      <div className={css.name}>{scrnshot.name}</div>
                      <div className={css.size}>{(scrnshot.size / 1024 / 1024).toFixed(2)} MB</div>
                    </div>
                    <div className="image-block">
                      <img src="https://khelbhai.in/images/global-cross.png" width="10px" alt="" onClick={() => setScrnshot(null)} />
                    </div>
                  </div>}
                </div>}
                {status !== null && status == 'cancelled' && <div class="form-group">
                  <textarea class="form-control border-secondary" name="" id="" rows="3" onChange={(e) => { setReason(e.target.value) }} required></textarea>
                </div>}
                {/* <button type='submit' className='btn btn-danger mt-3 text-white' id="post" onSubmit={(e) => { e.preventDefault(); Result() }}>Post Result</button> */}


                {fecthStatus == null && fecthStatus == undefined && < input type='submit' className='btn btn-danger mt-3 text-white' id="post" />}



              </form>
            </div>
          </div>
        </div>
      </div>}
      <div class='divider-y'></div>
      <div className='rightContainer'>
        <Rightcontainer/>
      </div>
    </>
  )
}
