import React, { useEffect, useState } from "react";
import axios from 'axios'
import { Link, useHistory, useLocation } from 'react-router-dom'

import '../css/landing.css'
import { Collapse } from 'react-bootstrap';
import Header from '../Components/Header';
import Rightcontainer from "../Components/Rightcontainer";
// import { BsChevronDown,BsChevronUp } from 'react-icons/bs';
// import 'animate.css';


import { w3cwebsocket as W3CWebSocket } from "websocket";

// const client = new W3CWebSocket('wss://cfrc-v5-services.ludokingapi.com/v5/socket.io/?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MjVkMTEyMTRlNTZiYTM5MWU2N2YyNmYiLCJlVXNlclR5cGUiOjIsImlhdCI6MTY1MjM0OTg5OX0.CM8L5QyDXqtKQmIpKD4R3sQWwjpwtWiw6cZ6FV-qVNI&bConnectAfterLogin=1&nVersion=5.2&nDevice=5&nVersionCode=171&pkgid=com.ludoking.fb&EIO=3&transport=websocket');


export default function Games() {

    // useEffect(() => {
    //     client.onopen = (data) => {
    //         // console.log('WebSocket Client Connected');
    //         client.send(JSON.stringify(420["LUDO_private_room",{"gameType":0,"nChipsToAdd":0,"nGemsToAdd":0,"iMaxPlayer":4,"iLobbyChips":100,"iLobbyWinAmount":300,"iLobbyId":1,"_userId":"625d11214e56ba391e67f26f","bUndo":0,"vPrivateCode":"newTable","bCheckOtherTable":false}]))


    //     };
    //     client.onmessage = (message) => {
    //         console.dir(message.data);

    //     };

    // }, []
    // )


    const history = useHistory()
    // console.log(history.location.state.from);
    const location = useLocation();
    const path = location.pathname.split("/")[2];
    // alert(path);
    const [games, setGames] = useState()
    const allGames = async () => {
        let access_token = localStorage.getItem('token')
        access_token = localStorage.getItem('token')
        const headers = {
            Authorization: `Bearer ${access_token}`
        }
        axios.get(`http://64.227.186.66:4000/game/type`, { headers })
            .then((res) => {
                setGames(res.data)
            })
            .catch((e) => {
                alert(e)
            })
    }
    /// user details start


    const [user, setUser] = useState()

    const role = async () => {
        const access_token = localStorage.getItem("token")
        const headers = {
            Authorization: `Bearer ${access_token}`
        }
        await axios.get(`http://64.227.186.66:4000/me`, { headers })
            .then((res) => {
                setUser(res.data._id)
                console.log(res.data._id)
            })

    }

    /// user details end
    useEffect(() => {
        role()
        allGames();
    }, [])
    const [open, setOpen] = useState(false);

    if (games == undefined) {
        return null
    }

    return (
        <>
            <div className='leftContainer'>
                {!user && <div className="headerContainer">
                    <Link to="/">
                        <picture className="ml-2 navLogo d-flex"><img src="https://res.cloudinary.com/crunchbase-production/image/upload/c_lpad,h_256,w_256,f_auto,q_auto:eco,dpr_1/tbvbvipimh2camf5nb2q" alt="" /></picture>
                    </Link>
                    <div className="menu-items">
                        <Link type="button" className="login-btn" to="/login">LOGIN</Link>
                    </div>
                </div>}
                {user && <Header />}
                <div className='main-area' style={{ paddingTop: '60px' }}>
                    <section className="games-section p-3">
                        <div className="d-flex align-items-center games-section-title">Our Games</div>
                        <div className="games-section-headline mt-2 mb-1  ">
                            {/* <img src="/images/global-purple-battleIcon.png" alt="" />
                             is for Battles and 
                             <img className="ml-1" src="/images/global-blue-tournamentIcon.png" alt="" /> 
                             is for Tournaments.
                            <span>Know more here.</span> */}
                            <div className="games-window ">


                                <Link className="gameCard-container" to={`/landing/${games[0]._id}`}>
                                    <blink className="d-none blink text-danger d-block text-right">◉ LIVE</blink>
                                    <a className="gameCard" href="/playground/ludo">
                                        <picture className="gameCard-image">
                                            <img width="100%" src={process.env.PUBLIC_URL + '/Images/LandingPage_img/ludo.jpeg'} height="173" alt="" />
                                        </picture>
                                        <div className="gameCard-title">Ludo</div>
                                        <picture className="gameCard-icon">
                                            <img src="/images/global-battleIconWhiteStroke.png" alt="" />
                                        </picture>
                                    </a>
                                </Link>
                                <div className="gameCard-container mt-3">
                                    {/* <span className="d-none blink text-danger d-block text-right">◉ LIVE</span> */}
                                    <a className="gameCard"  onClick={()=>alert('coming soon')}>
                                        <picture className="gameCard-image">
                                            <img width="100%" src={process.env.PUBLIC_URL + '/Images/LandingPage_img/snake.jpeg'} height="175px" alt="" />
                                        </picture>
                                        <div className="gameCard-title">Snack</div>
                                        <picture className="gameCard-icon">
                                            <img src="/images/global-battleIconWhiteStroke.png" alt="" />
                                        </picture>
                                    </a>
                                </div>
                                <div className="gameCard-container mt-2">
                                    {/* <span className="d-none blink text-danger d-block text-right">◉ LIVE</span> */}
                                    <a className="gameCard" onClick={()=>alert('coming soon')}>
                                        <picture className="gameCard-image">
                                            <img width="100%" src={process.env.PUBLIC_URL + '/Images/LandingPage_img/carrom.jpeg'} alt="" />
                                        </picture>
                                        <div className="gameCard-title">Carrom</div>
                                        <picture className="gameCard-icon">
                                            <img src="/images/global-battleIconWhiteStroke.png" alt="" />
                                        </picture>
                                    </a>
                                </div>
                                <div className="gameCard-container mt-2">
                                    {/* <span className="d-none blink text-danger d-block text-right">◉ LIVE</span> */}
                                    <a className="gameCard" onClick={()=>alert('coming soon')}>
                                        <picture className="gameCard-image">
                                            <img width="100%" src={process.env.PUBLIC_URL + '/Images/LandingPage_img/icon.webp'} alt="" />
                                        </picture>
                                        <div className="gameCard-title">Call Break</div>
                                        <picture className="gameCard-icon">
                                            <img src="/images/global-battleIconWhiteStroke.png" alt="" />
                                        </picture>
                                    </a>
                                </div>


                            </div>
                        </div>
                    </section>
                    <section className="footer">
                        <div className="footer-divider" />
                        <a className="px-3 py-4 d-flex align-items-center" href="#!" style={{ textDecoration: 'none' }} onClick={() => setOpen(!open)} aria-controls="example-collapse-text" aria-expanded={open}>

                            <picture className>
                                <img width="120px" height="55px" src={process.env.PUBLIC_URL + '/Images/LandingPage_img/game_icon.png'} alt="" />
                            </picture>
                            <span style={{ color: 'rgb(149, 149, 149)', fontSize: '1em', fontWeight: 400 }} className={!open ? 'd-block' : 'd-none'}> . Terms, Privacy, Support</span>

                            {open ? <i className="mdi mdi-chevron-up ml-auto" style={{ fontSize: '1.7em', color: 'rgb(103, 103, 103)' }}></i> : <i style={{ fontSize: '1.7em', color: 'rgb(103, 103, 103)' }} className="mdi mdi-chevron-down ml-auto"></i>}

                        </a>
                        <Collapse in={open}>
                            <div id="example-collapse-text" className="px-3 overflow-hidden">
                                <div className="row footer-links">
                                    <Link className="col-6" to="/term-condition">Terms &amp; Condition</Link>
                                    <Link className="col-6" to="/privacy-policy">Privacy Policy</Link>
                                    <Link className="col-6" to="/refund-policy">Refund/Cancellation Policy</Link>
                                    <Link className="col-6" to="/contact-us">Contact Us</Link>
                                    <Link className="col-6" to="/responsible-gaming">Responsible Gaming</Link>
                                </div>
                            </div>
                        </Collapse>
                        <div className="footer-divider" />
                        <div className="px-3 py-4">
                            <div className="footer-text-bold">About Us</div><br />
                            <div className="footer-text">KhelBhai is a real-money gaming product owned and operated by KhelBhai Private Limited
                                ("KhelBhai" or "We" or "Us" or "Our").</div><br />
                            <div className="footer-text-bold">Our Business &amp; Products</div><br />
                            <div className="footer-text">We are an HTML5 game-publishing company and our mission is to make accessing games
                                fast and easy by removing the friction of app-installs.</div><br />
                            <div className="footer-text">KhelBhai is a skill-based real-money gaming platform accessible only for our users
                                in India. It is accessible on <a target="_blank" rel="noopener noreferrer"
                                    href="https://www.khelbhai.in">https://www.khelbhai.in</a>. On KhelBhai, users can compete for real cash
                                in Tournaments and Battles. They can encash their winnings via popular options such as Paytm Wallet, Amazon
                                Pay, Bank Transfer, Mobile Recharges etc.</div><br />
                            <div className="footer-text-bold">Our Games</div><br />
                            <div className="footer-text">KhelBhai has a wide-variety of high-quality, premium HTML5 games. Our games are
                                especially compressed and optimised to work on low-end devices, uncommon browsers, and patchy internet
                                speeds.</div><br />
                            <div className="footer-text">We have games across several popular categories: Arcade, Action, Adventure, Sports
                                &amp; Racing, Strategy, Puzzle &amp; Logic. We also have a strong portfolio of multiplayer games such as
                                Ludo, Chess, 8 Ball Pool, Carrom, Tic Tac Toe, Archery, Quiz, Chinese Checkers and more! Some of our popular
                                titles are: Escape Run, Bubble Wipeout, Tower Twist, Cricket Gunda, Ludo With Friends. If you have any
                                suggestions around new games that we should add or if you are a game developer yourself and want to work
                                with us, don't hesitate to drop in a line at <a href="mailto:info@khelbhai.in">info@khelbhai.in</a>!</div>
                        </div>
                    </section>
                </div>
            </div>
            <div className='rightContainer'>
                <Rightcontainer/>
            </div>
        </>
    )
}
