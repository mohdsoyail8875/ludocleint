import axios from "axios";
import React, { useState } from "react";
import { useHistory, useLocation } from "react-router-dom";
import deposits from "../../app/components/transaction/Deposits";
import css from "../css/Addcase.module.css";
import { Link } from 'react-router-dom';
import Header from "../Components/Header";
import Rightcontainer from "../Components/Rightcontainer";

const Addcase = () => {

  const [global, setGlobal] = useState(" ");


  const deposit = () => {

    const access_token = localStorage.getItem("token")
    const headers = {
      Authorization: `Bearer ${access_token}`
    }
    axios.post(`http://64.227.186.66:4000/user/deposite`,
      { amount: global },
      { headers })
      .then((res) => {
        // console.log(res.data);

        window.location.href = res.data
      }).catch((e) => {
        alert(e)
      })
  }
  return (
    <>
      <div className="leftContainer">
        <Header />
        <div className="px-4 my-5  py-5">
          <div className={`${css.games_section}`}>
            <div className="d-flex position-relative align-items-center">
              <div className={`${css.games_section_title}`}>
                Choose amount to add
              </div>
            </div>
          </div>
          <div className="pb-3">
            <div className="MultiFormControl_root mt-4 MuiFormControl-fullWidth">
              <div className="MuiFormControl_root MuiTextField-root">
                <label
                  className={`${css.MuiFormLabel_root} MuiInputLabel-root MuiInputLabel-formControl MuiInputLabel-animated MuiInputLabel-shrink`}
                  data-shrink="true"
                  style={{ fontSize: "0.8rem", fontWeight: "400" }}
                >
                  Enter Amount
                </label>
                <div className="MuiInputBase-root MuiInput-root MuiInput_underline jss58 MuiInputBase-formControl MuiInput-formControl MuiInputBase-adornedStart">
                  <div className="MuiInputAdornment-root MuiInputAdornment-positionStart">
                    <input
                      class="w3-input input"
                      type="text"
                      style={{ width: "100%" }}
                      placeholder={`₹${global}`}
                      onChange={(e) => setGlobal(e.target.value)}
                    ></input>
                  </div>
                </div>
                <p className="MuiFormHelperText-root">Min: 10, Max: 50000</p>
              </div>
            </div>
            <div className={`${css.games_window}`}>
              <div
                className={`${css.gameCard_container}`}
                onClick={() => {
                  // console.log(100);
                  setGlobal(100);
                }}
              >
                <div className={`${css.add_fund_box}`}>
                  <div style={{ display: "flex", alignItems: "baseline" }}>
                    <div
                      className={`${css.collapseCard_title} mr-1`}
                      style={{ fontSize: "0.9em" }}
                    >
                      ₹
                    </div>
                    <div
                      className={`${css.collapseCard_title}`}
                      style={{ fontSize: "1.5em" }}
                    >
                      100
                    </div>
                  </div>
                </div>
              </div>
              <div
                className={`${css.gameCard_container}`}
                onClick={() => {
                  // console.log(250);
                  setGlobal(250);
                }}
              >
                <div className={`${css.add_fund_box}`}>
                  <div style={{ display: "flex", alignItems: "baseline" }}>
                    <div
                      className={`${css.collapseCard_title} mr-1`}
                      style={{ fontSize: "0.9em" }}
                    >
                      ₹
                    </div>
                    <div
                      className={`${css.collapseCard_title}`}
                      style={{ fontSize: "1.5em" }}
                    >
                      250
                    </div>
                  </div>
                </div>
              </div>
              <div
                className={`${css.gameCard_container}`}
                onClick={() => {
                  // console.log(500);
                  setGlobal(500);
                }}
              >
                <div className={`${css.add_fund_box}`}>
                  <div style={{ display: "flex", alignItems: "baseline" }}>
                    <div
                      className={`${css.collapseCard_title} mr-1`}
                      style={{ fontSize: "0.9em" }}
                    >
                      ₹
                    </div>
                    <div
                      className={`${css.collapseCard_title}`}
                      style={{ fontSize: "1.5em" }}
                    >
                      500
                    </div>
                  </div>
                </div>
              </div>
              <div
                className={`${css.gameCard_container}`}
                onClick={() => {
                  // console.log(1000);
                  setGlobal(1000);
                }}
              >
                <div className={`${css.add_fund_box}`}>
                  <div style={{ display: "flex", alignItems: "baseline" }}>
                    <div
                      className={`${css.collapseCard_title} mr-1`}
                      style={{ fontSize: "0.9em" }}
                    >
                      ₹
                    </div>
                    <div
                      className={`${css.collapseCard_title}`}
                      style={{ fontSize: "1.5em" }}
                    >
                      1000
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className={`${css.refer_footer}`}>
            <div class="d-grid gap-2 col-6 mx-auto">

              <button type="button" class={`${css.block}`} onClick={deposit}>Next</button>

            </div>
          </div>
        </div>
      </div>
      <div className="rightContainer">
        <Rightcontainer />
      </div>
    </>
  );
};
export default Addcase;
