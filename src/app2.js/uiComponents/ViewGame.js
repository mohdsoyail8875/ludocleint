import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useHistory, useLocation } from 'react-router-dom';
import '../css/viewGame.css'
import Header from '../Components/Header';
import Footer from '../Components/Footer';
import Scrollbanner from '../Components/Scrollbanner';
import Walletchips from '../Components/Walletchips';
import socketIOClient from "socket.io-client";
import Rightcontainer from '../Components/Rightcontainer';


export default function ViewGame() {



    const socket = socketIOClient("http://64.227.186.66:4000");

    const location = useLocation();
    const path = location.pathname.split("/")[2];

    const history = useHistory()
    const [Game, setGame] = useState()

    const randomcode = () => {
        const code = Math.floor((Math.random() * 1000000) + 1)
        return code
    }

    const getPost = async () => {
        const access_token = localStorage.getItem("token")
        const headers = {
            Authorization: `Bearer ${access_token}`
        }
        await axios.patch(`http://64.227.186.66:4000/challange/roomcode/${path}`,
            {
                Room_code: randomcode(),
                Status: "running"
            }
            , { headers })
            .then((res) => {
                setGame(res.data)
                socket.emit('challengeOngoing', res.data);
            })
            .catch((e) => alert(e))
    }
    /// user details start


    const [user, setUser] = useState()

    const role = async () => {
        const access_token = localStorage.getItem("token")
        const headers = {
            Authorization: `Bearer ${access_token}`
        }
        await axios.get(`http://64.227.186.66:4000/me`, { headers })
            .then((res) => {
                setUser(res.data._id)
                console.log(res.data._id)
                Allgames(res.data._id)

            })

    }

    /// user details end

    const [ALL, setALL] = useState()


    const Allgames = async (userId) => {
        const access_token = localStorage.getItem('token')
        const headers = {
            Authorization: `Bearer ${access_token}`
        }

        await axios.get(`http://64.227.186.66:4000/challange/${path}`, { headers })
            .then((res) => {
                setALL(res.data)
                setGame(res.data)
                console.log(res.data.Created_by._id)
                // if (userId == res.data.Created_by._id && res.data.Status == 'requested' && res.data.Status !== 'ongoing')
                // getPost()
            })
    }

    useEffect(() => {

        role();
        Allgames();
    }, [])


    // Result

    const [status, setStatus] = useState('winn');

    const Result = async () => {
        console.log(status)
        const access_token = localStorage.getItem("token")
        // console.log(access_token)
        const headers = {
            Authorization: `Bearer ${access_token}`
        }
        console.log(headers);
        await axios.patch(`http://64.227.186.66:4000/challange/result/${path}`,
            {
                status: status
            },
            { headers })
            .then((res) => {
                getPost(res.data)
                console.log(res.data);
                history.push("/Games")
            }).catch((e) => {
                console.log(e);
            })
    }



    const copyCode = (e) => {
        navigator.clipboard.writeText(Game.Room_code)
        // console.dir(e.target);
    }



    // if (Game == undefined) {
    //     return null
    // }

    // if (ALL == undefined) {
    //     return null
    // }



    return (
        <>
            {Game && <div className='leftContainer'>
                <Header />
                <Scrollbanner />
                <Walletchips />
                <div className="container">
                    <div className="col-12 mx-auto mt-3">
                        <p className="snip-p">
                            <font color="#ff0000">
                                <span style={{ backgroundColor: 'rgb(247, 247, 247)' }}>
                                    <b>
                                        Notice:1
                                    </b>
                                </span>
                            </font>
                        </p>
                        <p className="snip-p">
                            <font color="#ff0000">
                                <b>
                                    👉Start Recording From Enter Room Code Or Click Start in Ludo King.
                                </b>
                            </font>
                        </p>
                        <p className="snip-p">
                            <b style={{}}>
                                <font color="#6ba54a">
                                    👉Use.
                                </font>
                                <font color="#0000ff">
                                    AZ Recorder&nbsp;
                                </font>
                                <font color="#6ba54a">
                                    App to Record Game.
                                </font>
                            </b>
                        </p>
                        <p className="snip-p">
                            <font color="#ff0000">
                                <b>
                                    Notice:2
                                </b>
                            </font>
                        </p>
                        <p className="snip-p">
                            <b style={{}}>
                                <font color="#ff0000">
                                    👉
                                </font>
                                <font color="#6badde">
                                    ₹50. Penality = Wrong Update.
                                </font>
                            </b>
                        </p>
                        <p className="snip-p">
                            <font color="#6badde" style={{}}>
                                <b>
                                    👉 ₹25. Penality = if You Don't Update After Losing. ( गेम हारने के बाद लॉस&nbsp;अपडेट डालना जरूरी है)
                                </b>
                            </font>
                        </p>
                        <p className="snip-p">
                            <span style={{ backgroundColor: 'rgb(255, 255, 255)' }}>
                                Request- Game ko Game ki bhavna se khelo,kisi ko gaali&nbsp; mat kro,Respect doge Respect Milegi.
                            </span>
                            <font color="#6badde" style={{}}>
                                <b>
                                    <br />
                                </b>
                            </font>
                        </p>
                    </div>

                    <div className="col-12 mx-auto">
                        <h4 className="match-title text-center text-uppercase mt-3 mb-0 snip-h4">
                            <span className="text-success">
                                {/* {Game.Created_by.Name} */}
                            </span>
                        </h4>
                        <h5 className="match-title text-center text-uppercase mt-0 snip-h5">
                            has set this challenge
                        </h5>
                        <table className="table table-sm table-bordered text-center">
                            <thead className="bg-light">
                                <tr>
                                    <th>
                                        Game Type
                                    </th>
                                    <th>
                                        Amount
                                    </th>
                                    <th>
                                        Challanged Time
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td className="text-success">
                                        {/* {Game.Game_type} */}
                                    </td>
                                    <td className="text-success">
                                        {/* Rs.{Game.Game_Ammount} */}
                                    </td>
                                    <td className="text-info">
                                        {/* {Game.createdAt} */}
                                    </td>
                                </tr>
                                <tr>
                                    <td colSpan={3} className="text-danger">
                                        Winning Amount: Rs. 190
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div className="row mx-0">
                            <div className="col-3 p-0">
                                <button type="button" className="btn btn-secondary btn-block wrn-btn snip-button">
                                    <span style={{ fontSize: '9px' }}>ROOM CODE</span>
                                </button>
                            </div>
                            <div className="col-6 p-0">
                                <input type="text" className="form-control search-slt text-center font-weight-bold" name="room_code" id="room_code" readOnly value={Game.Room_code} />
                            </div>
                            <div className="col-3 p-0">
                                <button type="button" className="btn btn-info btn-block wrn-btn" onClick={(e) => copyCode(e)}>
                                    <span style={{ fontSize: '9px' }}>COPY CODE</span>
                                </button>
                            </div>
                            <div className="col-12 p-0 my-5 text-center ">
                                <h3 className="mt-3 snip-h3">
                                    UPDTE GAME STATUS
                                </h3>
                                <form encType="multipart/form-data">
                                    <div className="form-check-inline border border-success px-2" id="win">
                                        <label className="form-check-label text-success" htmlFor="radio1">
                                            <input type="radio" className="form-check-input" id="radio1" name="status" defaultValue="win" defaultChecked onClick={(e) => setStatus("winn")} />
                                            I WIN
                                        </label>
                                    </div>
                                    <div className="form-check-inline border border-danger px-2" id="lost">
                                        <label className="form-check-label text-danger" htmlFor="radio2">
                                            <input type="radio" className="form-check-input" id="radio2" name="status" defaultValue="lost" onClick={(e) => setStatus("lose")} />
                                            I LOST
                                        </label>
                                    </div>
                                    <div className="form-check-inline border border-warning px-2" id="cancel">
                                        <label className="form-check-label text-warning" htmlFor="radio3">
                                            <input type="radio" className="form-check-input" id="radio3" name="status" defaultValue="cancel" onClick={(e) => setStatus("cancelled")} />
                                            CANCEL GAME
                                        </label>
                                    </div>
                                    <div className="form-group mt-3" id="sh">
                                        <label htmlFor="comment">
                                            SCREENSHOT
                                        </label>
                                        <div className="custom-file" id="customFile" lang="es">
                                            <input type="file" className="custom-file-input" name="screenshot" id="screenshot" aria-describedby="fileHelp" accept=".png, .jpg, .jpeg" />
                                            <label className="custom-file-label text-left" htmlFor="screenshot">
                                                Select file...
                                            </label>
                                        </div>
                                    </div>
                                    <div className="form-group mt-3 text-center cbdiv">
                                        <label htmlFor="comment">
                                            Screenshot Preview
                                        </label>
                                        <img id="preview" className="img-thumbnail snip-img" style={{ width: '100%', height: 'auto' }} />
                                    </div>
                                    <div className="form-group mt-3" id="r" style={{ display: 'none' }}>
                                        <label htmlFor="comment">
                                            REASON OF CANCELLATION
                                        </label>
                                        <textarea className="form-control" rows={5} id="reason" name="reason" minLength={5} defaultValue={""} />
                                    </div>
                                    <div className="form-group mt-3 cbdiv">
                                        <label className="form-check-label text-left">
                                            <input type="checkbox" id="cb" />
                                            <small>
                                                I have verified the room code
                                            </small>
                                        </label>
                                    </div>  x
                                    <div className="form-group mt-3">
                                        <button className="btn btn-success snip-button" onClick={(e) => { e.preventDefault(); Result() }}>
                                            POST RESULT
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>}
            <div className='rightContainer'>
            <Rightcontainer/>
            </div>

        </>
    )
}
