import React from "react";
import Header from "../Components/Header";
import Rightcontainer from "../Components/Rightcontainer";

const Support = () => {
  return (
    <div>
        <div className="leftContainer">

    <div><Header/></div>

      <div className="cxy flex-column mx-4" style={{ paddingTop: "25%" }}>
        <img src={process.env.PUBLIC_URL + '/Images/contact_us.png'} width="280px" alt="" />
        <div className="games-section-title mt-4" style={{ fontSize: "1.2em",fontWeight:'700',color:'2c2c2c' }}>
          Contact us at below platforms.
        </div>
        <div className="mt-4 d-flex justify-content-around w-80">
          <a className="cxy flex-column" href="https://t.me/">
            <img width="50px" src={process.env.PUBLIC_URL + '/Images/telegram.png'} alt="" />
            <span className="footer-text-bold">Telegram</span>
          </a>
        </div>
        <span className="font-9 mt-3 mb-2">OR</span>
        <div className="mt-2 d-flex justify-content-around w-80">
          <a className="cxy flex-column" href="mailto:info@ludo.com">
            <img width="50px" src={process.env.PUBLIC_URL + '/Images/mail.png'} alt="" />
            <span className="footer-text-bold">info@khelbhai.in</span>
          </a>
        </div>
      </div>
        </div>
        <div className="rightContainer">
            <Rightcontainer />
        </div>
    </div>
  );
};

export default Support;
