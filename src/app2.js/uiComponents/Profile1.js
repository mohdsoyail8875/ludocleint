import React, { useEffect, useState } from 'react'
import "../css/layout.css"
import css from "../css/Profile.module.css"
import { Link, useHistory } from 'react-router-dom';
import SwipeableViews from 'react-swipeable-views';
import Header from '../Components/Header';
import axios from 'axios';
// import { Link, useHistory } from 'react-router-dom';
import Profile from './Profile';
import { Image } from 'react-bootstrap';
const Profile1 = () => {
    const [Id, setId] = useState(null)
    console.log(Id);
    const [profile, setProfile] = useState()
    const [portcss, setPortcss] = useState(css.active_tab)
    const [portcss1, setPortcss1] = useState(css.inactive_tab)
    const [crushcss, setCrushcss] = useState(true)
    const history = useHistory()
    const logout = () => {
        let access_token = localStorage.getItem("token")
        const headers = {
            Authorization: `Bearer ${access_token}`
        }
        axios.post(`http://64.227.186.66:4000/logoutAll`, {
            headers: headers
        }, { headers })
            .then((res) => {
                // setUser(res.data)
                history.push('/Games')
                localStorage.removeItem("token")
                window.location.reload()
            }).catch((e) => {
                // alert(e.msg)
            })
    }
    ///avatar
    const [avatar, setAvatar] = useState()
    const Avatar = async () => {
        const access_token = localStorage.getItem("token")
        // console.log(access_token)
        const headers = {
            Authorization: `Bearer ${access_token}`
        }
        const formData = new FormData();
        formData.append('avatar', avatar);
        await fetch(`http://64.227.186.66:4000/users/me/avatar`, {
            method: "POST",
            body: formData,
            headers
        }).then((res) => {
            setAvatar(res.data)
        }).catch((e) => {
            console.log(e);
        })
    }
    const UserALL = () => {
        let access_token = localStorage.getItem("token")
        const headers = {
            Authorization: `Bearer ${access_token}`
        }
        axios.get(`http://64.227.186.66:4000/me`, { headers })
            .then((res) => {
                setProfile(res.data)
                setId(res.data._id);
                TotalGame(res.data._id);
                images(res.data._id, res.data.avatar);
                console.log(res.data.avatar)
            }).catch((e) => {
                // alert(e.msg)
            })
    }
    const [Name, setName] = useState()
    const UpdateProfile = async () => {
        const access_token = localStorage.getItem("token")
        const headers = {
            Authorization: `Bearer ${access_token}`
        }
        const data = await axios.patch(`http://64.227.186.66:4000/user/edit/${Id}`, {
            Name,
        }, { headers })
            .then((res) => {
                // console.log(res.data)
                setName(res.data)
            })
    }
    //// total game
    const [total, setTotal] = useState()
    const TotalGame = (Id) => {
        let access_token = localStorage.getItem("token")
        const headers = {
            Authorization: `Bearer ${access_token}`
        }
        console.log('imran', Id);
        axios.get(`http://64.227.186.66:4000/total/user/all/${Id}`, { headers })
            .then((res) => {
                setTotal(res.data)
                // console.log(res.data);
            }).catch((e) => {
                // alert(e.msg)
            })
    }
    //avatar
    const [image, setavatar] = useState()
    const images = (Id, avatar) => {
        console.log(Id)
        let access_token = localStorage.getItem("token")
        const headers = {
            Authorization: `Bearer ${access_token}`
        }
        // console.log('imran', Id);
        axios.get(`http://64.227.186.66:4000/users/avatar/${Id}`, { headers })
            .then((res) => {
                const base64String = btoa(String.fromCharCode(...new Uint8Array(avatar)));
                setavatar(base64String)
                console.log(base64String);
            }).catch((e) => {
                // alert(e.msg)
            })
    }
    useEffect(() => {
        UserALL()
        // if(Id)
        // TotalGame(Id);
    }, [])
    if (profile == undefined) {
        return null
    }
    if (total == undefined) {
        return null
    }
    return (
        <>
            <div className="leftContainer">
                <div><Header /></div>
                <div className="pt-5" style={{ background: 'rgb(250, 250, 250)' }}>
                    <div className={`${css.center_xy} py-5`}>
                        {/* <input type="file" onChange={(e) => setAvatar(e.target.files[0])} required />
                        <button onClick={Avatar}>post</button> */}
                        <picture >
                            {/* {image} */}
                            <img height="80px" width="80px" src="https://icones.pro/wp-content/uploads/2021/02/icone-utilisateur-rouge.png" alt="" className="" />
                        </picture>
                        <span className={`${css.battle_input_header} mr-1`}>
                            {profile.Phone}
                        </span>
                        <div className={`text-bold my-3 ${portcss} font-weight-bold `} style={{ fontSize: '1em' }} >
                            {profile.Name}
                            <img className={`ml-2`} width="20px" src="https://khelbro.com/images/icon-edit.jpg" alt=""
                                onClick={() => {
                                    setPortcss(css.inactive_tab)
                                    setPortcss1(css.active_tab)
                                }}
                            />
                        </div>
                        <div className={`text-bold my-3 ${portcss1}`}>
                            <div className={`${css.MuiFormControl_root} ${css.MuiTextField_root}`} style={{ verticalAlign: 'bottom' }}>
                                <div className={`${css.MuiInputBase_root} ${css.MuiInput_root} ${css.MuiInput_underline} ${css.MuiInputBase_formControl} ${css.MuiInput_formControl}`}>
                                    <input aria-invalid="false" type="text" className={`${css.MuiInputBase_input} ${css.MuiInput_input}`} placeholder="Enter Username"
                                        onChange={(e) => {
                                            setProfile(e.target.value)
                                        }}
                                    />
                                </div>
                            </div>
                            <img className="ml-2" width="20px" src="https://khelbro.com/images/select-blue-checkIcon.png" alt=""
                                onClick={() => {
                                    setPortcss(css.active_tab)
                                    setPortcss1(css.inactive_tab)
                                    // UpdateProfile(Id)
                                }}
                            />
                        </div>
                        <Link className={`${css.profile_wallet} d-flex align-items-center`} style={{ width: '90%' }} to="/wallet">
                            <picture className="ml-4">
                                <img width="32px" src="https://khelbro.com/images/sidebar-wallet.png" alt="" />
                            </picture>
                            <div className={`${css.mytext} ml-5 text-muted`}>
                                My Wallet
                            </div>
                        </Link>
                    </div>
                </div>
                <div className={css.divider_x}>
                </div>
                <div className="p-3" style={{ background: 'white' }}>
                    <div className={css.text_bold}>
                        Complete Profile
                    </div>
                    <div className="d-flex">
                        <svg className={`${css.MuiSvgIcon_root} ${css.MuiSvgIcon_colorAction}`} focusable="false" viewBox="0 0 24 24" aria-hidden="true" style={{ height: 'unset' }}
                            onClick={() => {
                                setCrushcss(!crushcss)
                                console.log(crushcss)
                            }}
                        >
                            <path d="M11.67 3.87L9.9 2.1 0 12l9.9 9.9 1.77-1.77L3.54 12z">
                            </path>
                        </svg>
                        <div style={{ overflowX: 'hidden' }}>
                            <div className="react-swipeable-view-container" style={{ flexDirection: 'row', transition: 'all 0s ease 0s', direction: 'ltr', display: 'flex', willChange: 'transform', transform: 'translate(0%, 0px)' }}>
                                <div aria-hidden={!crushcss} data-swipeable="true" style={{ width: '100%', flexShrink: 0, overflow: 'auto' }} className="">
                                    <Link className={`d-flex align-items-center ${css.profile_wallet} bg-light mx-1 my-4 py-5`} style={{ backgroundColor: 'whitesmoke' }} to={profile.verified == `unverified` ? `/kyc2` : null}>
                                        <picture className="ml-4">
                                            <img width="32px" src="https://khelbro.com/images/kyc-icon-new.png" alt="" className="" />
                                        </picture>
                                        <div className={`ml-5 ${css.mytext} text-muted`}>
                                            {profile.verified == `unverified` ? 'Complete KYC' : profile.verified == 'pending' ? 'In Process' : 'KYC COMPLETED ✅'}
                                        </div>
                                    </Link>
                                </div>
                                <div aria-hidden={crushcss} data-swipeable="true" style={{ width: '100%', flexShrink: 0, overflow: 'auto' }} className="">
                                    <Link className={`d-flex align-items-center ${css.profile_wallet} bg-light mx-1 my-4 py-3`} to="/profile1">
                                        <picture className="ml-4">
                                            <img width="32px" src="https://khelbro.com/images/mail.png" alt="" className="snip-img" />
                                        </picture>
                                        <div className="ml-5 mytext text-muted snip-div">
                                            EMAIL UPDATED ✅
                                        </div>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        <svg className={`${css.MuiSvgIcon_root} ${css.MuiSvgIcon_colorAction}`} focusable="false" viewBox="0 0 24 24" aria-hidden="true" style={{ height: 'unset' }}
                            onClick={() => {
                                setCrushcss(!crushcss)
                            }}
                        >
                            <path d="M5.88 4.12L13.76 12l-7.88 7.88L8 22l10-10L8 2z">
                            </path>
                        </svg>
                    </div>
                </div>
                <div className={css.divider_x}>
                </div>
                <div className="px-3 py-1">
                    <div className={`d-flex align-items-center ${css.position_relative}`} style={{ height: '84px' }}>
                        <picture>
                            <img height="32px" width="32px" src="https://khelbro.com/images/global-cash-won-green-circular.png" alt="" className="snip-img" />
                        </picture>
                        <div className="pl-4">
                            <div className={`${css.text_uppercase} ${css.moneyBox_header}`} style={{ fontSize: '0.8em' }}>
                                Cash Won
                            </div>
                            <div className="">
                                <picture className="mr-1">
                                    <img height="auto" width="21px" src="https://khelbro.com/images/global-rupeeIcon.png" alt="" className="snip-img" />
                                </picture>
                                <span className={css.moneyBox_text} style={{ fontSize: '1em', bottom: '-1px' }}>
                                    {profile.wonAmount}
                                </span>
                            </div>
                            <span className={css.thin_divider_x}>
                            </span>
                        </div>
                    </div>
                </div>
                <div className="px-3 py-1">
                    <div className={`d-flex align-items-center ${css.position_relative}`} style={{ height: '84px' }}>
                        <picture>
                            <img height="32px" width="32px" src="https://khelbro.com/images/global-purple-battleIcon.png" alt="" className="" />
                        </picture>
                        <div className="pl-4">
                            <div className={`${css.text_uppercase} ${css.moneyBox_header}`} style={{ fontSize: '0.8em' }}>
                                Battle Played
                            </div>
                            <div className="snip-div">
                                <span className={css.moneyBox_text} style={{ fontSize: '1em', bottom: '-1px' }}>
                                    {total}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="px-3 py-1">
                    <div className={`d-flex align-items-center ${css.position_relative}`} style={{ height: '84px' }}>
                        <picture>
                            <img height="32px" width="32px" src="https://khelbro.com/images/referral-signup-bonus-new.png" alt="" className="snip-img" />
                        </picture>
                        <div className="pl-4">
                            <div className={`${css.text_uppercase} ${css.moneyBox_header}`} style={{ fontSize: '0.8em' }}>
                                Referral Earning
                            </div>
                            <div className="">
                                <span className={css.moneyBox_text} style={{ fontSize: '1em', bottom: '-1px' }}>
                                    {profile.referral_earning}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={css.divider_x}>
                </div>
                <div className="p-3 snipcss-A1eLC snip-div">
                    <Link to="#!" className={`${css.center_xy}  text-uppercase py-4 font-weight-bolder text-muted`} onClick={(e) => logout(e)}>
                        Log Out
                    </Link>
                </div>
            </div>
            <div className='rightContainer'>
                <div className="rcBanner flex-center ">
                    <picture className="rcBanner-img-container animate__bounce infinite ">
                        <img src="https://i.pinimg.com/originals/72/3d/0a/723d0af616b1fe7d5c7e56a3532be3cd.png" alt="" />
                    </picture>
                    <div className="rcBanner-text ">Khel Bro <span className="rcBanner-text-bold">Win Real Cash!</span></div>
                    <div className="rcBanner-footer">For best experience, open&nbsp;<a href="#!" style={{
                        color: 'rgb(44, 44, 44)',
                        fontWeight: 500, textDecoration: 'none'
                    }}>KhelBhai.com</a>&nbsp;on&nbsp;<img src="https://khelbro.com/images/global-chrome.png"
                        alt="" />&nbsp;chrome mobile</div>
                </div>
            </div>
            {/* <ModalProfile style3={
                profileModalstyle
            } Enter={Enter}/> */}
            <div className={css.kyc_select} id="profileModal" >
                <div className={css.overlay} />
                <div
                    className={`${css.box}`}
                    style={{
                        bottom: '0px',
                        position: 'absolute',
                    }}
                >
                    <div className={css.bg_white}>
                        <div className={`${css.header} ${css.cxy} ${css.flex_column}`}>
                            <picture>
                                <img
                                    height="80px"
                                    width="80px"
                                    src="https://khelbro.com/images/avatars/Avatar2.png"
                                    alt=""
                                />
                            </picture>
                            <div className={`${css.header_text} mt-2`}>Choose Avatar</div>
                        </div>
                        <div className="mx-3 pb-3" style={{ paddingTop: "200px" }}>
                            <div className="row justify-content-between col-10 mx-auto">
                                <img
                                    height="50px"
                                    width="50px"
                                    src="https://khelbro.com/images/avatars/Avatar1.png"
                                    alt=""
                                />
                                <img
                                    height="50px"
                                    width="50px"
                                    src="https://khelbro.com/images/avatars/Avatar2.png"
                                    alt=""
                                />
                                <img
                                    height="50px"
                                    width="50px"
                                    src="https://khelbro.com/images/avatars/Avatar3.png"
                                    alt=""
                                />
                                <img
                                    height="50px"
                                    width="50px"
                                    src="https://khelbro.com/images/avatars/Avatar4.png"
                                    alt=""
                                />
                            </div>
                            <div className="row justify-content-between col-10 mx-auto mt-3">
                                <img
                                    height="50px"
                                    width="50px"
                                    src="https://khelbro.com/images/avatars/Avatar5.png"
                                    alt=""
                                />
                                <img
                                    height="50px"
                                    width="50px"
                                    src="https://khelbro.com/images/avatars/Avatar6.png"
                                    alt=""
                                />
                                <img
                                    height="50px"
                                    width="50px"
                                    src="https://khelbro.comimages/avatars/Avatar7.png"
                                    alt=""
                                />
                                <img
                                    height="50px"
                                    width="50px"
                                    src="https://khelbro.com/images/avatars/Avatar8.png"
                                    alt=""
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default Profile1













