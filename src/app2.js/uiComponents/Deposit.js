import React from 'react'
import '../css/layout.css'
import '../css/homepage.css'
import Header from '../Components/Header'
import Footer from '../Components/Footer'
import Scrollbanner from '../Components/Scrollbanner'
import Walletchips from '../Components/Walletchips'
import { Link } from 'react-router-dom';
import Rightcontainer from '../Components/Rightcontainer'

export default function Deposite() {
    return (
        <>
        <div className='leftContainer'>
            <Header/>
            {/* <div> <Scrollbanner/> </div>
            <div><Walletchips/></div> */}

            <div className="container px-3">
                <div className="row">
                    <div className="col-md-8 mx-auto">
                    <a href="/transaction-history" className="btn btn-info btn-block mt-3">View Deposit Request &amp; History</a>
                    <div className="card text-center mt-3">
                        <div className="card-header py-1 bg-success text-white"><h4 className="mb-0">Buy Chips</h4></div>
                        <div className="card-body">
                        <h1><b style={{}}><font style={{backgroundColor: 'rgb(255, 156, 0)'}} color="#ffffff">&nbsp; 1 Rupee = 1 Chip&nbsp;&nbsp;</font></b></h1><h4><font color="#ffffff"><span style={{backgroundColor: 'rgb(156, 0, 255)'}}><b>&nbsp;instant Deposit&nbsp;</b></span></font></h4><h4><b>Deposit Via Phone Pe/PayTM.</b></h4><ul><li><b>Select PhonePe Or PayTM.</b></li><li><b>Copy Number.&nbsp;</b></li><li><b>Pay via PayTM Or Phone pe.&nbsp;</b></li><li><b>Enter Txn. ID: Or Order ID:</b></li><li><b>Enter Amount.&nbsp;</b></li><li><b>Click Submit Now.&nbsp;</b></li><li><b>(Instant deposit only phone pay)</b></li></ul><p /><h3 /><h1 />              
                        <div className>
                            <form action="/transaction-history" method="post" className="p-3">
                            <input type="hidden" name="_token" defaultValue="O8fQt2dIwzuZT6zee4bFpi9dHcIEXiQ7Um0hL03R" />                    <div className="row border bg-light p-3">
                                <div className="col-12 p-0">
                                <div className="row">
                                    <div className="col-8 pl-3 p-0">
                                    <input type="text" className="form-control" id="upi_no" placeholder="Select UPI App First" defaultValue readOnly />
                                    </div>
                                    <div className="col-4 pr-3 p-0">
                                    <button type="button" className="btn btn-info btn-block" onclick="copyToClipboard();">COPY</button>
                                    </div>
                                </div>
                                </div>
                                <div className="col-12 p-0 mt-1">
                                <select className="form-control search-slt" id="upi_app" name="upi_app" onchange="selectUPI();" required>
                                    <option value>Select UPI App</option>
                                    <option value="PayTM">PayTM</option>
                                    <option value="PhonePe">PhonePe</option>
                                </select>
                                </div>
                                <div className="col-md-12 p-0 mt-1">
                                <input type="text" className="form-control search-slt" name="txn_id" placeholder="Transaction Id" defaultValue required="required" />
                                </div>
                                <div className="col-12 p-0 mt-1">
                                <input type="number" className="form-control search-slt" name="amount" placeholder="Amount" defaultValue required="required" autoComplete="off" />
                                </div>
                                <div className="col-12 p-0">
                                <button type="submit" className="btn btn-info btn-block btn-sm">SUBMIT NOW</button>
                                </div>
                            </div>
                            </form>
                            <div className="border bg-light p-3 text-center">
                            <h6 className="font-weight-bold">For Any Query</h6>
                            Please contact support at whatsapp (0000000000)<br />
                            <a href="https://api.whatsapp.com/send?phone=000000000&text=Hi admin, I need help.&app_absent=0" target="_blank" rel="noreferrer"><i className="fab fa-whatsapp text-success" /> Click here to contact Admin</a>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            {/* <Footer/> */}
        </div>
        <div className='rightContainer'>
            <Rightcontainer/>
        </div>
        </>
    )
}