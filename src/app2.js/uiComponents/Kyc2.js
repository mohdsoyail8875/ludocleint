import React, { useEffect, useState } from "react";
import Header from "../Components/Header";
import "../css/kyc.css";
import Rightcontainer from "../Components/Rightcontainer";
import { Link, useHistory } from "react-router-dom";
import axios from "axios";
import css from '../css/Pan.module.css'

const Kyc2 = () => {
  const history = useHistory()

  const [frontLoaded, setfrontLoaded] = useState(null)
  const [backLoaded, setbackLoaded] = useState(null)
  const [Name, setName] = useState()
  const [Number, setNumber] = useState()
  const [DOB, setDob] = useState()

  const handleSubmitdata = (e) => {

    e.preventDefault();
    console.log(frontLoaded, backLoaded)
    const formData = new FormData();

    formData.append("Name", Name);
    formData.append("Number", Number);
    formData.append("DOB", DOB);
    formData.append("front", frontLoaded);
    formData.append("back", backLoaded);

    if (frontLoaded && backLoaded) {
      const access_token = localStorage.getItem('token')
      const headers = {
        Authorization: `Bearer ${access_token}`,
      }

      axios.post(`http://64.227.186.66:4000/aadharcard`, formData, { headers })
        .then((res) => {
          // console.log(res.data)
          history.push("/profile1")

        }).catch((e) => {
          console.log(e);
        })
    }
    else {
      alert('please fill all field')
    }



  }
  useEffect(() => {
    const frontPhoto = document.getElementById('frontPhoto');
    frontPhoto.onchange = e => {
      const [file] = frontPhoto.files;
      setfrontLoaded(file)
    }
    const backPhoto = document.getElementById('backPhoto');
    backPhoto.onchange = e => {
      const [file] = backPhoto.files;
      setbackLoaded(file)
    }
  }, [])
  return (
    <div>
      <div className="leftContainer">
        <Header />

        <div className="kycPage mt-5 py-4 px-3">
          <p className="mt-2" style={{ color: "rgb(149, 149, 149)" }}>
            You need to submit a document that shows that you are{" "}
            <span style={{ fontWeight: 500 }}>above 18 years</span> of age and not a
            resident of{" "}
            <span style={{ fontWeight: 500 }}>
              Assam, Odisha, Sikkim, Nagaland, Telangana, Andhra Pradesh, Tamil Nadu and
              Karnataka.
            </span>
          </p>
          {/* <br /> */}
          {/* <div>
            <span style={{ fontSize: "1.5em" }}>Step 2</span> of 3
          </div> */}
          <p className="mt-2" style={{ color: "rgb(149, 149, 149)" }}>
            Enter details of Aadhaar Card:{" "}
            {/* <span style={{ fontWeight: 500 }}>098765432212</span> */}
          </p>
          {/* <br /> */}
          <div style={{ marginTop: "10px" }}>
            <div className="kyc-doc-input">
              <div className="label">First Name</div>
              <input type="text"
                name="Name"
                placeholder='enter name'
                onChange={(e) => setName(e.target.value)} required
              />
            </div>
            {/* <br /> */}
            {/* <div className="kyc-doc-input mt-4">
              <div className="label">Last Name</div>
              <input
                type="text"
                name="lastName"
                autoComplete="off"
                defaultValue
                value=""
                placeholder="enter First Name"
              />
            </div> */}
            <div className="kyc-doc-input mt-4">
              <div className="label">Date of Birth</div>
              <input type="date"
                name="Name"
                placeholder='enter name'
                onChange={(e) => setName(e.target.value)} required
              />
            </div>
            <div className="kyc-doc-input mt-4">
              <div className="label">Aadhar Number</div>
              <input type="Number"
                name="Name"
                placeholder=' Aadhar Number'
                onChange={(e) => setNumber(e.target.value)} required
              />
            </div>
            <div className={`${css.doc_upload} mt-4`}>
              <input id="frontPhoto" name="frontPhoto" type="file" accept="image/*" required />
              {!frontLoaded && <div className="cxy flex-column position-absolute ">
                <img src="https://khelbro.com/images/file-uploader-icon.png" width="17px" alt="" className="snip-img" />
                <div className={`${css.sideNav_text} mt-2`}>
                  Upload front Photo of your Aadhar Card.
                </div>
              </div>}
              {frontLoaded && <div className={css.uploaded}>
                <img src="https://khelbro.com/images/file-icon.png" width="26px" alt="" style={{ marginRight: '20px' }} />
                <div className="d-flex flex-column" style={{ width: '80%' }}>
                  <div className={css.name}>{frontLoaded.name}</div>
                  <div className={css.size}>{(frontLoaded.size / 1024 / 1024).toFixed(2)} MB</div>
                </div>
                <div className="image-block">
                  <img src="https://khelbro.com/images/global-cross.png" width="10px" alt="" onClick={() => setfrontLoaded(null)} />
                </div>
              </div>}
            </div>
            <div className={`${css.doc_upload} mt-4`}>
              <input id="backPhoto" name="backPhoto" type="file" accept="image/*" required />
              {!backLoaded && <div className="cxy flex-column position-absolute ">
                <img src="https://khelbro.com/images/file-uploader-icon.png" width="17px" alt="" className="snip-img" />
                <div className={`${css.sideNav_text} mt-2`}>
                  Upload back Photo of your Aadhar Card.
                </div>
              </div>}
              {backLoaded && <div className={css.uploaded}>
                <img src="https://khelbro.com/images/file-icon.png" width="26px" alt="" style={{ marginRight: '20px' }} />
                <div className="d-flex flex-column" style={{ width: '80%' }}>
                  <div className={css.name}>{backLoaded.name}</div>
                  <div className={css.size}>{(backLoaded.size / 1024 / 1024).toFixed(2)} MB</div>
                </div>
                <div className="image-block">
                  <img src="https://khelbro.com/images/global-cross.png" width="10px" alt="" onClick={() => setbackLoaded(null)} />
                </div>
              </div>}
            </div>
            {/* <div style={{ marginTop: "50px" }}>
              <span style={{ color: "rgb(149, 149, 149)", fontWeight: 500 }}>
                State
              </span>
              <div className="kyc-input mt-2">
                <div className="kyc-input-text">Select State</div>
                <div className="arrow cxy">
                  <img
                    src="/images/global-black-chevronDown.png"
                    width="15px"
                    alt=""
                  />
                </div>
              </div>
            </div> */}
            <div className="kyc-select">
              <div className="overlay" style={{}} />
              <div
                className="box kyc-select-exit-done"
                style={{ bottom: "0px", position: "absolute" }}
              >
                <div className="bg-white">
                  <div className="header">
                    <div className="d-flex position-relative align-items-center">
                      <div className="header-text">Select State</div>
                    </div>
                  </div>
                  <div style={{ paddingTop: "80px" }}>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">Arunachal Pradesh</div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">Bihar</div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">Chhattisgarh</div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">Goa</div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">Gujarat</div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">Haryana</div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">Himachal Pradesh</div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">Jammu and Kashmir</div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">Jharkhand</div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">Kerala</div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">Karnataka</div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">Madhya Pradesh</div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">Maharashtra</div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">Manipur</div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">Mizoram</div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">Punjab</div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">Rajasthan</div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">Tripura</div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">Tamil Nadu</div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">Uttarakhand</div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">Uttar Pradesh</div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">West Bengal</div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">
                        Andaman and Nicobar Islands
                      </div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">Chandigarh</div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">Dadra and Nagar Haveli</div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">Daman and Diu</div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">Delhi</div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">Lakshadweep</div>
                    </div>
                    <div className="option d-flex align-items-center">
                      <div className="option-text">Puducherry</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div style={{ paddingBottom: "25%" }} />
          <div className="refer-footer p-0">
            {/* <button className="refer-button btn-success mr-2  w-100 " style={{ background: '#6c757d !important' }}>
              <a href="/kyc">Previous</a>
            </button> */}
            <button onClick={handleSubmitdata} className="w-100 btn-success bg-success" style={{
              border: 'none', borderRadius: '5px',
              fontSize: '1em',
              fontWeight: '700',
              height: '48px',
              color: '#fff',
              textTransform: 'uppercase',
            }}>
              {/* <Link  >Next</Link> */}
              submit
            </button>
          </div>
        </div>
      </div>
      <div className="rightContainer">
        <Rightcontainer />
      </div>
    </div>
  );
};

export default Kyc2;
