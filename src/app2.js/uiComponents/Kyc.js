import React,{useState} from 'react'
import "../css/kyc.css";
import Header from '../Components/Header'
import Rightcontainer from '../Components/Rightcontainer';
import { Link } from 'react-router-dom';
import css from "../css/Profile.module.css"

// const open = () => {
//     alert("open is not working");
//     // const width=document.getElementById("kycSelect").offsetWidth;
//     document.getElementById("kycSelect").style.bottom = "block";
//     // document.getElementById('sidebarOverlay').style.display = 'none';
  
//   };


const Kyc = () => {

    
    const [value, setValue] = useState('Select Document');
    const [profileModal, setProfileModal] = useState(false)
    const [overlayStyle, setOverlayStyle] = useState({})
    const [Enter, setEnter] = useState(css.kyc_select_exit_done)
    const handleModal = () => {
        setProfileModal(!profileModal)
        // // setProfileModalstyle({pointerEvents: 'auto', opacity: '0.87'})
        setOverlayStyle({pointerEvents: 'auto', opacity: '0.87'})
        setEnter(css.kyc_select_enter_done)
        console.log(profileModal)
    }
    const rehandleModal = () => {
        setProfileModal(!profileModal)
        setOverlayStyle({})
        setEnter(css.kyc_select_exit_done)
        document.getElementById('kycSelect').style.display = 'block';
    }

  return (
    <div>
        <div className='leftContainer'>
            <Header/>
            <div className="kycPage mt-5 py-5 px-3">
            <div>
                <span style={{ fontSize: "1.5em" }}>Step 1</span> of 3
            </div>
            <p className="mt-2" style={{ color: "rgb(149, 149, 149)" }}>
                You need to submit a document that shows that you are{" "}
                <span style={{ fontWeight: 500 }}>above 18 years</span> of age and not a
                resident of{" "}
                <span style={{ fontWeight: 500 }}>
                Assam, Odisha, Sikkim, Nagaland, Telangana, Andhra Pradesh, Tamil Nadu and
                Karnataka.
                </span>
            </p>
            <br />
            <div>
                <span style={{ color: "rgb(149, 149, 149)", fontWeight: 500 }}>
                Document  Type
                </span>
                {/* <div>
                <select className="form-select mt-2" aria-label="Default select example">
                    <option selected>Select document</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                </select>
                </div> */}
           
                 <button className="kyc-input mt-2" onClick={handleModal}>
                    <div className="kyc-input-text">{
                        value
                    }</div>
                    <div className="arrow cxy">
                        <img src="https:khelbhai.in/images/global-black-chevronDown.png" width="15px" alt="" />
                    </div>
                </button>
                <div style={{marginTop: '50px', display:'none',}} id="kycSelectInput">
                    <div className="kyc-doc-input">
                        <div className="label">{`Enter ${value} Number`}
                        </div>
                        <input type="text" name="docValue" autocomplete="off" placeholder={`Enter ${value} Number`} Required />
                    </div>
                </div>
            </div>
            <div className="kyc-select" id="kycSelect" onClick={rehandleModal}>
                <div className='overlay' style={overlayStyle} />
                <div
                    className={`box ${Enter}`}
                    style={{
                        bottom: '0px',
                        position: 'absolute',
                    }}
                >
                <div className={css.bg_white}>
                    <div className={`header ${css.cxy} ${css.flex_column}`}>
                    <div className="d-flex position-relative align-items-center">
                        <div className="header-text">
                            Document Type
                        </div>
                    </div>
                    </div>
                    <div style={{ paddingTop: "80px" }}>
                    <div className="option d-flex align-items-center">
                        <div className="option-text" onClick={() => setValue('Aadhar Card')}>
                        Aadhaar Card</div>
                    </div>
                    <div className="option d-flex align-items-center">
                        <div className="option-text" onClick={() => {setValue('Driving License')
                            document.getElementById('kycSelectInput').style.display = 'block';
                        }}>
                        Driving License</div>
                    </div>
                    <div className="option d-flex align-items-center">
                        <div className="option-text" onClick={() => setValue('Voter ID')}>
                        Voter ID Card</div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            <div style={{ paddingBottom: "80%" }} />
            <div className="refer-footer">
                <button className="refer-button w-100" >
                    <Link to='/kyc2' style={{color:'#fff !important'}}>Next</Link>
                </button>
            </div>
            </div>
        </div>
        <div className='rightContainer'>
            <Rightcontainer/>
        </div>
    </div>
  )
}

export default Kyc





