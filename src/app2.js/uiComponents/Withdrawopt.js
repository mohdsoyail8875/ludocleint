import React from 'react'
import Header from '../Components/Header'
import Rightcontainer from '../Components/Rightcontainer'

 const 
 Withdrawopt = () => {
  return (
    <>
    <div className="leftContainer bg-white">
        <Header />
    <div class="Mywallet_divider_x__YSrQ5 XXsnipcss_extracted_selector_selectionXX snipcss0-0-0-1 tether-target-attached-top tether-abutted tether-abutted-top tether-element-attached-top tether-element-attached-center tether-target-attached-center mt-5"></div>
    <div class="px-4 py-3">
        <div class="games-section">
            <div class="d-flex position-relative align-items-center">
                <div class="games-section-title">Choose withdrawal option</div>
                </div>
                </div>
                <div class="mt-3">
                    <div class="add-fund-box d-flex align-items-center mt-4" style={{backgroundColor:"#fafafa",width:"auto",border:"1px solid grey",borderRadius:"8px",paddingTop: "15px", height: "60px"}}>
                        <div class="d-flex align-items-center">
                            <img width="48px" src={process.env.PUBLIC_URL+'Images/upi.webp'} alt="" style={{marginLeft:"7px",paddingBottom:"10px",paddingLeft:"3px"}}/>
                            <div class="d-flex justify-content-center flex-column ml-4">
                                <div class="jss7"><strong>Withdraw to UPI</strong></div>
                                <div class="jss8" style={{color:"#b36916",fontSize:"10px"}}>Minimum withdrawal amount ₹95</div>
                                </div>
                                </div>
                                </div>
                                <div class="add-fund-box d-flex align-items-center mt-4" style={{backgroundColor:"#fafafa",width:"auto",border:"1px solid grey",borderRadius:"8px",paddingTop: "15px", height: "60px"}}>
                                    <div class="d-flex align-items-center">
                                        <img width="48px" src={process.env.PUBLIC_URL+'Images/bank.png'} alt=""style={{marginLeft:"7px",paddingBottom:"10px",paddingLeft:"3px"}}/>
                                        <div class="d-flex justify-content-center flex-column ml-4">
                                            <div class="jss7"><strong>Bank Transfer</strong></div>
                                            <div class="jss8"style={{color:"#b36916",fontSize:"10px"}}>Minimum withdrawal amount ₹95</div>
                                            </div>
                                            </div>
                                            </div>
                                            </div>
                                            </div>
    </div>
    <div class="divider-y"></div>
    <div className="rightContainer">
        <Rightcontainer />
    </div>
    </>
  )
}
export default Withdrawopt