import React, { useEffect, useState } from 'react'
import "../css/layout.css"
import "../css/Profile.css"
import Walletchips from '../Components/Walletchips'
import Scrollbanner from '../Components/Scrollbanner'
import axios from 'axios'
import Header from "../Components/Header";
import Footer from "../Components/Footer";

const Profile = () => {


  const [Name, setName] = useState()
  const [Id, setId] = useState()

  const [Phone, setPhone] = useState()
  const [LKID, setLKID] = useState()


  const getData = async () => {
    const access_token = localStorage.getItem("token")

    const headers = {
      Authorization: `Bearer ${access_token}`
    }
    const data = await axios.get(`http://64.227.186.66:4000/me`, { headers })
      .then((res) => {
        setLKID(res.data.LKID)
        setName(res.data.Name)
        setPhone(res.data.Phone)
        setId(res.data._id)


        console.log(res.data);
      })
  }

  useEffect(() => {
    getData()
  }, [])





  const getPost = async () => {
    // alert(Name)
    const access_token = localStorage.getItem("token")
    const headers = {
      Authorization: `Bearer ${access_token}`
    }
    const data = await axios.patch(`http://64.227.186.66:4000/user/edit/${Id}`, {
      Name,
      Phone,
      LKID,
    }, { headers })
      .then((res) => {
        // console.log(res.data)
        setPhone(res.data.Phone)
        setName(res.data.Name)
        setLKID(res.data.LKID)
      })
    // .catch((e) => alert(e))
  }



  // update Password

  const [Password, setPassword] = useState()
  const [newPassword, setNewPassword] = useState()
  const [confirmNewPassword, setConfirmNewPassword] = useState()

  const UpdatePassword = async () => {
    // alert(Name)
    const access_token = localStorage.getItem("token")
    const headers = {
      Authorization: `Bearer ${access_token}`
    }
    const data = await axios.post(`http://64.227.186.66:4000/changepassword`, {
      Password,
      newPassword,
      confirmNewPassword,
    }, { headers })
      .then((res) => {
        alert("Password Updated")
      })

      .catch((e) => alert("something went wrong"))
  }

  // useEffect(() => {
  //   getPost()
  // }, [])




  return (
    <div>
      <div className="leftContainer">
        <div>
          <Header />
        </div>
        <div>
          {" "}
          <Scrollbanner />{" "}
        </div>
        <div>
          <Walletchips />
        </div>
        <div className="container px-3">
          <div className="row">
            <div className="col-md-12 mx-auto">
              <hr />
              <h4 className="text-info">
                <small>
                  <strong>PROFILE UPDATE</strong>
                </small>
              </h4>
              <hr />
              <div className="row">
                <div className="col-md-12">
                  <div className="card">
                    <div className="card-header">
                      Update Profile
                    </div>
                    <div className="card-body">
                      <form >
                        {/* <input type="hidden" name="_token" defaultValue="tl15cSQ8xO6MVlzBQP35Vvzd4fOanWFsqWBBMF3h" /> */}
                        <div className="form-group">
                          <label htmlFor="mobile">Mobile</label>
                          <input type="text" className="form-control" id="mobile" onChange={(e) => setPhone(e.target.value)} value={Phone} />
                        </div>
                        <div className="form-group">
                          <label htmlFor="name">Name</label>
                          <input type="text" className="form-control" id="name" name="name" placeholder="Name" onChange={(e) => setName(e.target.value)} value={Name} />
                        </div>
                        <div className="form-group">
                          <label htmlFor="name">Ludoking ID</label>
                          <input type="text" className="form-control" id="ludoking_id" name="ludoking_id" placeholder="Ludoking ID" onChange={(e) => setLKID(e.target.value)} value={LKID} />
                        </div>
                        <button type="submit" className="btn btn-primary" onClick={(e) => getPost(Id)}>UPDATE PROFILE</button>
                      </form>
                    </div>
                  </div>
                </div>
                <div className="col-md-12">
                  <div className="card">
                    <div className="card-header">
                      Update Password
                    </div>
                    <div className="card-body">
                      <form >
                        {/* <input type="hidden" name="_token" defaultValue="tl15cSQ8xO6MVlzBQP35Vvzd4fOanWFsqWBBMF3h" /> */}
                        <div className="form-group">
                          <label htmlFor="old_password">Old Password</label>

                          <input type="password" className="form-control" id="old_password" name="old_password" placeholder="Enter Old Password" onChange={(e) => setPassword(e.target.value)} />
                        </div>
                        <div className="form-group">
                          <label htmlFor="new_password">New Passowrd</label>
                          <input type="password" className="form-control" id="new_password" name="new_password" placeholder="Enter New Password" onChange={(e) => setNewPassword(e.target.value)} />
                        </div>
                        <div className="form-group">
                          <label htmlFor="confirm_password">Confirm Password</label>
                          <input type="password" className="form-control" id="confirm_password" name="confirm_password" placeholder="Enter Confirm Password" onChange={(e) => setConfirmNewPassword(e.target.value)} />
                        </div>
                        <button type="submit" className="btn btn-primary" onClick={(e) => UpdatePassword(e)}>UPDATE PASSWORD</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </div>
      <div className="rightContainer">Right</div>
    </div>
  );
};

export default Profile;
