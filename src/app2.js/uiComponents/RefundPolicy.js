import React from "react";
import { Link } from "react-router-dom";
import Header from "../Components/Header";
import Rightcontainer from "../Components/Rightcontainer";
import "../css/terms.css";

const RefundPolicy = () => {
  return (
    <div>
      <div className="leftContainer">
        <Header />
        <div className="mt-5 py-4 px-3">
          <div className="m-3">
            <h1>Refund Policy</h1>
            <nav aria-label="breadcrumb">
              <ol className="breadcrumb">
                <li className="breadcrumb-item">
                  <Link href="/" >Home</Link>
                </li>
                <li className="breadcrumb-item active" aria-current="page">
                  Refund Policy
                </li>
              </ol>
            </nav>
            <h4>Refund Policy</h4>
            <p>
              Thanks for being a patron with KhelBhai. If you are not entirely
              satisfied with your subscription, we are here to help.
            </p>
            <h4>Refund</h4>
            <p>
              Once we receive your Refund request, we will inspect it and notify
              you on the status of your refund.
            </p>
            <p>
              If your refund request is approved, we will initiate a refund to
              your credit card (or original method of payment) within 7 working
              days. You will receive the credit within a certain amount of days,
              depending on your card issuer's policies.
            </p>
            <p>
              In case of unforeseen technical glitch, KhelBhai would refund
              subscription upon reviewing the complaint. Final decision lies
              with the company.
            </p>
          </div>
        </div>
      </div>
      <div className="rightContainer">
        <Rightcontainer />
      </div>
    </div>
  );
};

export default RefundPolicy;
