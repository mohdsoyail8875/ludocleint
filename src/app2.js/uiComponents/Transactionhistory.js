import React, { useState, useEffect } from "react";
import css from "../css/gamehis.module.css";
import ReactPaginate from "react-paginate";
import Header from "../Components/Header";
import Rightcontainer from "../Components/Rightcontainer";


const cardData = [
  {
    id: 1,
    status: "Cash withdrawen in UPI.",
    player: "Wicket-Systri-Warrick",
    image: "https://khelbhai.in/images/games/ludo.jpeg",
    rupimage: "https://khelbhai.in/images/global-rupeeIcon.png",
    date: "20 apr",
    time: "10:00",
    battleId: "fasdjkfa423",
    balance: "100",
    amount: "10000",
  },
  {
    id: 2,
    status: "Redeemed refer balance",
    player: "Wicket-Systri-Warrick",
    image: "https://khelbhai.in/images/games/ludo.jpeg",
    rupimage: "https://khelbhai.in/images/global-rupeeIcon.png",
    date: "20 apr",
    time: "10:00",
    battleId: "fasdjkfa423",
    balance: "100",
    amount: "10000",
  },
  {
    id: 2,
    status: "Redeemed refer balance",
    player: "Wicket-Systri-Warrick",
    image: "https://khelbhai.in/images/games/ludo.jpeg",
    rupimage: "https://khelbhai.in/images/global-rupeeIcon.png",
    date: "20 apr",
    time: "10:00",
    battleId: "fasdjkfa423",
    balance: "100",
    amount: "10000",
  },
  {
    id: 2,
    status: "Redeemed refer balance",
    player: "Wicket-Systri-Warrick",
    image: "https://khelbhai.in/images/games/ludo.jpeg",
    rupimage: "https://khelbhai.in/images/global-rupeeIcon.png",
    date: "20 apr",
    time: "10:00",
    battleId: "fasdjkfa423",
    balance: "100",
    amount: "10000",
  },
  {
    id: 2,
    status: "Redeemed refer balance",
    player: "Wicket-Systri-Warrick",
    image: "https://khelbhai.in/images/games/ludo.jpeg",
    rupimage: "https://khelbhai.in/images/global-rupeeIcon.png",
    date: "20 apr",
    time: "10:00",
    battleId: "fasdjkfa423",
    balance: "100",
    amount: "10000",
  },
  {
    id: 2,
    status: "Redeemed refer balance",
    player: "Wicket-Systri-Warrick",
    image: "https://khelbhai.in/images/games/ludo.jpeg",
    rupimage: "https://khelbhai.in/images/global-rupeeIcon.png",
    date: "20 apr",
    time: "10:00",
    battleId: "fasdjkfa423",
    balance: "100",
    amount: "10000",
  },
  {
    id: 3,
    status: "Cash added using upi",
    player: "Wicket-Systri-Warrick",
    image: "https://khelbhai.in/images/games/ludo-popular.jpeg",
    date: "20 apr",
    time: "10:00",
    battleId: "fasdjkfa423",
  },
  {
    id: 3,
    status: "Redeemed refer balance",
    player: "Wicket-Systri-Warrick",
    image: "https://khelbhai.in/images/games/ludo-popular.jpeg",
    date: "20 apr",
    time: "10:00",
    battleId: "fasdjkfa423",
  },
  {
    id: 3,
    status: "Cash added using upi",
    player: "Wicket-Systri-Warrick",
    image: "https://khelbhai.in/images/games/ludo-popular.jpeg",
    date: "20 apr",
    time: "10:00",
    battleId: "fasdjkfa423",
  },
  {
    id: 3,
    status: "Cash withdrawen in UPI.",
    player: "Wicket-Systri-Warrick",
    image: "https://khelbhai.in/images/games/ludo-popular.jpeg",
    date: "20 apr",
    time: "10:00",
    battleId: "fasdjkfa423",
  },
  {
    id: 3,
    status: "Cash added using upi",
    player: "Wicket-Systri-Warrick",
    image: "https://khelbhai.in/images/games/ludo-popular.jpeg",
    date: "20 apr",
    time: "10:00",
    battleId: "fasdjkfa423",
  },
];

const Transactionhistory = () => {
  // onclick of the pagination
  // const [pageNumber, setPageNumber] = useState(0);

  // const usersPerPage = 10;
  // const pagesVisited = pageNumber * cardData;

  // const pageCount = Math.ceil(cardData.length / usersPerPage);

  // const changePage = ({ selected }) => {
  //     setPageNumber(selected);
  // };

  return (
    <div>
      <div className="leftContainer">
        {/* pagination */}
      <div><Header/></div>
        {/* <div className="pt-5"> */}
        <nav className={`d-flex ${css.MuiPagination_root} pt-3 justify-content-center snipcss-KGrAz snip-nav`}>
          {/* <ReactPaginate
                                        previousLabel={"<"}
                                        nextLabel={">"}
                                        pageCount={pageCount}
                                        onPageChange={changePage}
                                        containerClassName={"navigationButtons"}
                                        previousLinkClassName={"previousButton"}
                                        nextLinkClassName={"nextButton"}
                                        disabledClassName={"navigationDisabled"}
                                        activeClassName={"navigationActive"}

                                    /> */}
          <ul className={`${css.MuiPagination_ul}`}>
            <li className>
              <button
                className={`${css.MuiButtonBase_root} ${css.MuiPaginationItem_root} ${css.MuiPaginationItem_page} ${css.MuiPaginationItem_textSecondary} ${css.Mui_disabled}`}
                type="button"
                aria-label="Go to previous page"
              >
                <svg
                  className={`${css.MuiSvgIcon_root} ${css.MuiPaginationItem_icon} snip-svg`}
                  focusable="false"
                  viewBox="0 0 24 24"
                  aria-hidden="true"
                >
                  <path d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"></path>
                </svg>
              </button>
            </li>
            <li className>
              <button
                className={`${css.MuiButtonBase_root} ${css.MuiPaginationItem_root} ${css.MuiPaginationItem_page} ${css.MuiPaginationItem_textSecondary} ${css.Mui_selected}`}
                type="button"
                aria-label="page 1"
              >
                1<span className={`${css.MuiTouchRipple_root}`}></span>
              </button>
            </li>
            <li className>
              <button
                className={`${css.MuiButtonBase_root} ${css.MuiPaginationItem_root} ${css.MuiPaginationItem_page} ${css.MuiPaginationItem_textSecondary}`}
                type="button"
              >
                2<span className={`${css.MuiTouchRipple_root}`}></span>
              </button>
            </li>
            <li className>
              <button
                className={`${css.MuiButtonBase_root} ${css.MuiPaginationItem_root} ${css.MuiPaginationItem_page} ${css.MuiPaginationItem_textSecondary}`}
                type="button"
              >
                3<span className={`${css.MuiTouchRipple_root}`}></span>
              </button>
            </li>
            <li className>
              <button
                className={`${css.MuiButtonBase_root} ${css.MuiPaginationItem_root} ${css.MuiPaginationItem_page} ${css.MuiPaginationItem_textSecondary}`}
                type="button"
              >
                4<span className={`${css.MuiTouchRipple_root}`}></span>
              </button>
            </li>
            <li className>
              <button
                className={`${css.MuiButtonBase_root} ${css.MuiPaginationItem_root} ${css.MuiPaginationItem_page} ${css.MuiPaginationItem_textSecondary}`}
                type="button"
              >
                5<span className={`${css.MuiTouchRipple_root}`}></span>
              </button>
            </li>
            <li className>
              <button
                className={`${css.MuiButtonBase_root} ${css.MuiPaginationItem_root} ${css.MuiPaginationItem_page} ${css.MuiPaginationItem_textSecondary}`}
                type="button"
              >
                <svg
                  className={`${css.MuiSvgIcon_root} ${css.MuiPaginationItem_icon} snip-svg`}
                  focusable="false"
                  viewBox="0 0 24 24"
                  aria-hidden="true"
                >
                  <path d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"></path>
                </svg>
                <span className={`${css.MuiTouchRipple_root}`}></span>
              </button>
            </li>
          </ul>
        </nav>
        {/* </div> */}

        {/* game-cards */}
        {cardData.map((card) => {
          return (
            <div
              className={`w-100 py-3 d-flex align-items-center ${css.list_item}`}
            >
              {/* map the cardData */}
              <div className={`${css.center_xy} ${css.list_date} mx-2`}>
                <div>{card.date}</div>
                <small>{card.time}</small>
              </div>
              <div className={`${css.list_divider_y}`} />
              <div className={`mx-3 d-flex ${css.list_body}`}>
                <div className="d-flex align-items-center">
                  <picture className="mr-2">
                    <img
                      height="32px"
                      width="32px"
                      src={process.env.PUBLIC_URL + '/Images/LandingPage_img/ludo.jpeg'}
                      alt=""
                      style={{ borderRadius: "5px" }}
                    />
                  </picture>
                </div>

                <div className="d-flex flex-column font-8">
                  <div>
                    {card.status} <b>
                        {/* {card.player} */}
                        </b>.
                  </div>
                  <div className={`${css.games_section_headline}`}>
                    orderId:{card.battleId}
                  </div>
                </div>
              </div>

             <div className="right-0 d-flex align-items-end pr-3 flex-column">
                <button className={`btn btn-sm ${css.btn_success} ${css.status_badge}`}>
                  PAID
                </button>
                <div className="d-flex float-right font-8">
                  <div className="text-success">(+)</div>
                  <picture className="ml-1 mb-1">
                    <img
                      height="21px"
                      width="21px"
                      src={process.env.PUBLIC_URL + '/Images/LandingPage_img/global-rupeeIcon.png'}
                      className="snip-img"
                    />
                  </picture>
                  <span className="pl-1">500</span>
                </div>
                <div
                  className="games-section-headline"
                  style={{ fontSize: "0.6em" }}
                >
                  Closing Balance: 180
                </div>
              </div>
            </div>
          );
        })}
      </div>

      <div className="rightContainer">
        <Rightcontainer/>
      </div>
    </div>
  );
};

export default Transactionhistory;
