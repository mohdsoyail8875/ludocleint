import React from 'react'
import css from "../css/Refer.module.css"
import NavBar from '../Components/NavBar'
import Header from '../Components/Header'
import { Link } from 'react-router-dom'

const Refer = () => {
  return (
    <div>
      <div className='leftContainer'>
        <Header/>
      <div className={`${css.center_xy} pt-5`}>
  <picture className="mt-1">
    <img width="226px" src={process.env.PUBLIC_URL + 'Images/refer/refer.png'} className="snip-img" />
  </picture>
  <div className="mb-1">
    <div className="font-15">
      Earn now unlimited 
      <span aria-label="party-face" className>
        🥳
      </span>
    </div>
    <div className="d-flex justify-content-center">
      Refer your friends now!
    </div>
    <div className="mt-3 text-center font-9">
      Current Earning: 
      <b>
        ₹138.5
      </b>
      <Link className="ml-2" to="/refer">
        Redeem
      </Link>
    </div>
    <div className="text-center font-9">
      Total Earned: 
      <b>
        ₹6,417.5
      </b>
    </div>
    <div className={`${css.progress}`}>
      <div className={`${css.progress_bar} ${css.progress_bar_striped} ${css.bg_success }`}aria-valuenow="6417.5" aria-valuemax={10000} style={{width: '64.175%'}}>
      </div>
    </div>
    <div className="font-9">
      <span className>
        Max: ₹10,000
      </span>
      <Link className="float-right" to="/update-pan">
        Upgrade Limit
      </Link>
    </div>
    <div className={`${css.text_bold} mt-3 text-center`}>
      Your Refer Code: KWRTLGUR
      <img className="ml-2" width="20px" src={process.env.PUBLIC_URL + 'Images/refer/pen.jpeg'} />
    </div>
    <div className="d-flex justify-content-center">
      Total Refers:&nbsp;
      <b>
        10
      </b>
    </div>
  </div>
</div>

<div className="mx-3 my-3">
  <div className={`${css.font_11} ${css.text_bold}`}>
    Refer &amp; Earn Rules
  </div>
  <div className="d-flex align-items-center m-3">
    <picture>
      <img width="82px" src={process.env.PUBLIC_URL + 'Images/refer/giftbanner.png'} className="snip-img" />
    </picture>
    <div className={`${css.font_9} mx-3`} style={{width: '63%'}}>
      <div>
        When your friend signs up on KhelBhai from your referral link,
      </div>
      <div className={`${css.font_8} ${css.c_green} mt-2`}>
        You get 
        <strong>
          1% Commission
        </strong>
        on your 
        <strong>
          referral's winnings.
        </strong>
      </div>
    </div>
  </div>
  <div className="d-flex align-items-center m-3">
    <picture>
      <img width="82px" src={process.env.PUBLIC_URL + 'Images/refer/banner.png'} className="snip-img" />
    </picture>
    <div className={`${css.font_9} mx-3`} style={{width: '63%'}}>
      <div className>
        Suppose your referral plays a battle for ₹10000 Cash,
      </div>
      <div className={`${css.font_8} ${css.c_green} mt-2`}>
        You get 
        <strong>
          ₹100 Cash
        </strong>
        <strong>
        </strong>
      </div>
    </div>
  </div>
</div>

<div className={`${css.refer_footer} pt-4 `}>
{/* <a href=" https://wa.me/7297960021"> */}
  <button className="bg-green refer-button cxy w-100">
    Share in Whatsapp
  </button>
  {/* </a> */}

  <button className="refer-button-copy ml-2 d-flex">
    <picture>
      <img height="18px" width="18px" src={process.env.PUBLIC_URL + 'Images/refer/grey.png'} className="snip-img" />
    </picture>
  </button>
</div>

</div>
<div className='rightContainer'> right</div>

    </div>
  )
}

export default Refer
