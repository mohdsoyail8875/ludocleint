import axios from 'axios'
import React, { useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import Swal from 'sweetalert2'
import '../css/layout.css'
import '../css/login.css'

export default function Login() {

    const history = useHistory()


    const [Phone, setPhone] = useState();
    const [twofactor_code, settwofactor_code] = useState();
    const [otp, setOtp] = useState(false)

    // console.log(FirstName);
    const handleClick = async (e) => {
        e.preventDefault();

        if (!Phone) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Please enter your phone number',

            })
        } else {
            const { data } = await axios.post("http://64.227.186.66:4000/login", {
                Phone,
            }).then((respone) => {
                if(respone.data.msg === "User does not exist"){
                    // <div class="alert alert-warning" role="alert">
                    //     This is a warning alert—check it out!
                    // </div>
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'User does not exist',
                        // width: '20%',
                        // height:'20%',
                    })
                }else{
                setOtp(true)
                console.log(respone.data);
               
        }});
        }
    }


    const varifyOtp = async (e) => {
        e.preventDefault();
        if (!Phone) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Please enter your phone number',

            })
        } else {
            const { data } = await axios.post("http://64.227.186.66:4000/login/finish", {
                Phone,
                twofactor_code
            }).then((respone) => {
                if(respone.data.msg === "Invalid Login"){
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Invalid Login',
                        // width:'200px'
                    })
                }
                else{
                const token = respone.data.data
                localStorage.setItem("token", token)

                history.push("/Games")
                window.location.reload()
                // console.log(respone.data);
         } });
        }
    }
    return (
        <>
            <div className='leftContainer'>
                <div className="main-area bg-dark">
                    <div style={{ overflowY: 'hidden' }}>
                        <div className="splash-overlay" />
                        <div className="splash-screen animate__bounce infinite ">
                            <figure><img width="100%" src="https://play-lh.googleusercontent.com/DE3Pnn2lQ0vrudHmK8gKC_WKzXHNjUJ6mU1B76fX3hHQAhaVTtHL50a3AUrdbOZNaJWG" alt="" /></figure>
                        </div>
                        <div className="position-absolute w-100 center-xy mx-auto" style={{ top: '30%', zIndex: 3 }}>
                            <div className="d-flex text-white font-15 mb-4">Sign in or Sign up</div>
                            {!otp && <div className="bg-white px-3 cxy flex-column" style={{
                                width: '85%', height: '60px', borderRadius: '5px'
                            }}>
                                <div className="input-group mb-2 " style={{ transition: 'top 0.5s ease 0s', top: '5px' }}>
                                    <div className="input-group-prepend">
                                        <div className="input-group-text" style={{ width: '100px', backgroundColor: '#e9ecef', border: '1px solid #d8d6de' }}>+91</div>
                                    </div>
                                    <input className="form-control" name="mobile" type="mobile" placeholder="Mobile number"
                                        onChange={(e) => setPhone(e.target.value)}
                                        style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px" }}
                                    />
                                    {/* <div className="invalid-feedback">Enter a valid mobile number</div> */}
                                </div>

                            </div>}
                            {otp && <div className="bg-white px-3 cxy flex-column" style={{
                                width: '85%', height: '60px', borderRadius: '5px', marginTop: "10px"
                            }}>

                                <div className="input-group mb-2" style={{ transition: 'top 0.5s ease 0s', top: '5px' }}>
                                    <div className="input-group-prepend">
                                        {/* <div className="input-group-text" style={{ width: '100px' }}>+91</div> */}
                                    </div>
                                    <input className="form-control" name="password" type="text" placeholder="otp"
                                        onChange={(e) => settwofactor_code(e.target.value)}
                                        style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px", border: '1px solid #d8d6de' }} />
                                    {/* <div className="invalid-feedback">Enter a valid mobile number</div> */}
                                </div>

                            </div>}
                            {!otp && <button className="bg-green refer-button cxy mt-4" style={{ width: '85%' }} onClick={(e) => handleClick(e)} >Continue
                            </button>}
                            {otp && <button className="bg-green refer-button cxy mt-4" style={{ width: '85%' }} onClick={(e) => varifyOtp(e)} >Verify
                            </button>}
                            <Link className="bg-green refer-button cxy mt-4" style={{ width: '85%' }} to='register' >Register
                            </Link>
                        </div>
                        {/* <div className="login-footer">By proceeding, you agree to our <Link to="/terms">Terms of Use</Link>, <Link
                            to="/PrivacyPolicy">Privacy Policy</Link> and that you are 18 years or older. You are not playing from
                            Assam, Odisha, Nagaland, Sikkim, Meghalaya, Andhra Pradesh, or Telangana.</div> */}
                    </div>
                </div>

            </div>
            <div className='rightContainer'>
                <div className="rcBanner flex-center ">
                    <picture className="rcBanner-img-container animate__bounce infinite ">
                        <img src="https://i.pinimg.com/originals/72/3d/0a/723d0af616b1fe7d5c7e56a3532be3cd.png" alt="" />
                    </picture>
                    <div className="rcBanner-text ">Khel Bro <span className="rcBanner-text-bold">Win Real Cash!</span></div>
                    <div className="rcBanner-footer">For best experience, open&nbsp;<Link to="#!" style={{
                        color: 'rgb(44, 44, 44)',
                        fontWeight: 500, textDecoration: 'none'
                    }}>KhelBhai.com</Link>&nbsp;on&nbsp;<img src="https://khelbhai.in/images/global-chrome.png"
                        alt="" />&nbsp;chrome mobile</div>
                </div>
            </div>
        </>
    )
}
