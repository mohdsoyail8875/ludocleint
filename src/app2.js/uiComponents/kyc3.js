import React from "react";
import Header from "../Components/Header";
import "../css/kyc.css";
import Rightcontainer from "../Components/Rightcontainer";
import { Link } from "react-router-dom";


const Kyc3 = () => {
  return (
    <div>
      <div className="leftContainer">
        <Header />
        <div className="kycPage mt-5 py-4 px-3">
          <div>
            <span style={{ fontSize: "1.5em" }}>Step 3</span> of 3
          </div>
          <p className="mt-2" style={{ color: "rgb(149, 149, 149)" }}>
            Upload photo of Aadhaar Card:{" "}
            <span style={{ fontWeight: 500 }}>098765432212</span>
          </p>
          <br />
          <div style={{ marginTop: "10px" }}>
            <div className="mytext" style={{ fontSize: "1.1em" }}>
              Ensure that your <span style={{ fontWeight: 700 }}>name</span>,{" "}
              <span style={{ fontWeight: 700 }}>birthdate</span> and{" "}
              <span style={{ fontWeight: 700 }}>document number</span> are
              clearly visible in the document photo.
            </div>
          </div>
          <div style={{}} className="doc-upload mt-5">
            <input id="frontPic" name="frontPic" type="file" accept="image/*" />
            <div className="cxy flex-column position-absolute">
              <img src="https:khelbhai.in/images/file-uploader-icon.png" width="17px" alt="" />
              <div className="sideNav-text mt-2">Upload Front Photo</div>
            </div>
          </div>
          <div className="doc-upload mt-5">
            <input id="backPic" name="backPic" type="file" accept="image/*" />
            <div className="cxy flex-column position-absolute">
              <img src="https:khelbhai.in/images/file-uploader-icon.png" width="17px" alt="" />
              <div className="sideNav-text mt-2">Upload Back Photo</div>
            </div>
          </div>
          <div style={{ paddingBottom: "44%" }} />
          <div className="refer-footer">
            <button className="refer-button mr-2  w-100 ">
              <Link to="/kyc2">Previous</Link>
            </button>
            <button className="refer-button  w-100 ">
              Complete
              alert("Your KYC is completed");
            </button>
          </div>
        </div>
      </div>
      <div className="rightContainer">
      <Rightcontainer/>

      </div>
    </div>
  );
};

export default Kyc3;
