import axios from 'axios'
import React, { useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import Swal from 'sweetalert2'
// import withReactContent from 'sweetalert2-react-content'
import '../css/layout.css'
import '../css/register.css'
// import 'animate.css';




function Register() {

    const history = useHistory()

    const [Name, setName] = useState("")
    const [Phone, setPhone] = useState("")
    const [Email, setEmail] = useState("")
    const [Password, setPassword] = useState("")
    const [referral, setReferral] = useState("")
    const [referral_code, setreferral_code] = useState("")
    const [otp, setOpt] = useState(false)
    const [twofactor_code, settwofactor_code] = useState();

    const handleClick = async (e) => {
        // alert("kdsjf")
        e.preventDefault();
        if (!Name || !Password || !Email || !Phone) {
            Swal.fire({
                icon: 'error',
                title: 'oops..',
                text: 'Enter the correct details'
            })
        } else {
            const { data } = await axios.post("http://64.227.186.66:4000/register", {

                Name,
                Phone,
                Email,
                Password,
                referral,
                referral_code

            }).then((respone) => {
                if (respone.data.msg === "Phone have already") {
                    Swal.fire({
                        icon: 'error',
                        title: 'oops..',
                        text: 'You have already registered phone number'
                    })
                } else {

                    setOpt(true)
                    console.log(respone.data);
                }
            });
        }

    }
    const varify = async (e) => {
        e.preventDefault();
        if (!Name || !Password || !Email || !Phone || !twofactor_code) {
            Swal.fire({
                icon: 'error',
                title: 'oops..',
                text: 'Enter the correct otp'
            })
        } else {
            const { data } = await axios.post("http://64.227.186.66:4000/register/finish", {

                Name,
                Phone,
                Email,
                Password,
                referral,
                twofactor_code,
                referral_code

            }).then((respone) => {
                if (respone.data.msg == "success") {
                    const token = respone.data.token
                    localStorage.setItem("token", token)
                    // console.log(respone.data.token);
                    history.push("/Games")
                    window.location.reload()
                }
                else {
                    Swal.fire({
                        icon: 'error',
                        title: 'oops..',
                        text: 'Enter the  otp'
                    })
                }
            }).catch((err) => {
                alert(err)
            })
        }

    }
    return (
        <>
            <div className='leftContainer'>
                <div className="main-area">
                    <div style={{ overflowY: 'hidden' }}>
                        <div className="splash-overlay" />
                        <div className="splash-screen animate__bounce infinite">
                            <figure><img width="100%" className="" src="https://play-lh.googleusercontent.com/DE3Pnn2lQ0vrudHmK8gKC_WKzXHNjUJ6mU1B76fX3hHQAhaVTtHL50a3AUrdbOZNaJWG" alt="" /></figure>
                        </div>
                        <div className="position-absolute w-100 center-xy mx-auto" style={{ top: '13%', zIndex: 3 }}>
                            <div className="d-flex text-white font-15 mb-4">Sign in or Sign up</div>
                            {/* name */}
                            <div style={{ width: '100%', marginRight: '-14%' }} >
                                {!otp &&
                                    <div className="bg-white p-2 my-2 cxy flex-column" style={{ width: '85%', borderRadius: '5px' }}>
                                        <div className="input-group " style={{ transition: 'top 0.5s ease 0s', }}>
                                            <input className="form-control " name="Name" type="text" placeholder="Name"
                                                onChange={(e) => setName(e.target.value)}
                                                style={{ transition: 'all 3s ease-out 0s', border: '1px solid #d8d6de' }} />
                                        </div>

                                    </div>}

                                {!otp &&
                                    <div className="bg-white p-2 my-2 cxy flex-column" style={{ width: '85%', borderRadius: '5px' }}>
                                        <div className="input-group " style={{ transition: 'top 0.5s ease 0s', }}>
                                            <div className="input-group-prepend">
                                                <div className="input-group-text" style={{ width: '100px', backgroundColor: '#e9ecef', border: '1px solid #d8d6de' }}>+91</div>
                                            </div>
                                            <input className="form-control  " name="mobile" type="tel" placeholder="Mobile number"
                                                onChange={(e) => setPhone(e.target.value)}
                                                style={{ transition: 'all 3s ease-out 0s', border: '1px solid #d8d6de' }} />
                                        </div>
                                    </div>
                                }
                                {!otp &&
                                    <div className="bg-white p-2 my-2 cxy flex-column" style={{ width: '85%', borderRadius: '5px' }}>

                                        <div className="input-group" style={{ transition: 'top 0.5s ease 0s', }}>

                                            <input className="form-control  mt-2" name="email" type="email" placeholder="Email"
                                                onChange={(e) => setEmail(e.target.value)}

                                                style={{ transition: 'all 3s ease-out 0s', border: '1px solid #d8d6de' }} />
                                        </div>
                                    </div>
                                }
                                {!otp &&
                                    <div className="bg-white p-2 my-2 cxy flex-column" style={{ width: '85%', borderRadius: '5px' }}>

                                        <div className="input-group" style={{ transition: 'top 0.5s ease 0s', }}>

                                            <input className="form-control mt-2" name="password" type="password" placeholder="Password"
                                                onChange={(e) => setPassword(e.target.value)}

                                                style={{ transition: 'all 3s ease-out 0s', border: '1px solid #d8d6de' }} />
                                        </div>
                                    </div>
                                }
                                {!otp &&
                                    <div className="bg-white p-2 my-2 cxy flex-column" style={{ width: '85%', borderRadius: '5px' }}>

                                        <div className="input-group" style={{ transition: 'top 0.5s ease 0s', }}>

                                            <input className="form-control mt-2" name="text" type="text" placeholder="referral code"
                                                onChange={(e) => setReferral(e.target.value)}

                                                style={{ transition: 'all 3s ease-out 0s', border: '1px solid #d8d6de' }} />
                                        </div>
                                    </div>
                                }

                                {/* </div> */}

                                {otp && <div className="bg-white px-3 cxy flex-column" style={{
                                    width: '85%', height: '60px', borderRadius: '5px', marginTop: "10px"
                                }}>

                                    <div className="input-group mb-2" style={{ transition: 'top 0.5s ease 0s', top: '5px' }}>
                                        <div className="input-group-prepend">
                                            {/* <div className="input-group-text" style={{ width: '100px' }}>+91</div> */}
                                        </div>
                                        <input className="form-control" name="password" type="text" placeholder="otp"
                                            onChange={(e) => settwofactor_code(e.target.value)}
                                            style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px", border: '1px solid #d8d6de' }} />
                                        {/* <div className="invalid-feedback">Enter a valid mobile number</div> */}
                                    </div>

                                </div>}

                            </div>
                            {!otp && <button className="bg-green refer-button cxy mt-4" style={{ width: '85%' }} onClick={(e) => handleClick(e)} >Continue
                            </button>}
                            {otp && <button className="bg-green refer-button cxy mt-4" style={{ width: '85%' }} onClick={(e) => varify(e)} >Continue
                            </button>}
                            <Link className="bg-green refer-button cxy mt-4" style={{ width: '85%' }} to='login' >Login
                            </Link>
                        </div>
                        {/* <div className="login-footer">By proceeding, you agree to our <Link href="/terms">Terms of Use</Link>, <Link
                            to="/PrivacyPolicy">Privacy Policy</Link> and that you are 18 years or older. You are not playing from
                            Assam, Odisha, Nagaland, Sikkim, Meghalaya, Andhra Pradesh, or Telangana.</div> */}
                    </div>
                </div>

            </div>
            <div className='rightContainer'>
                <div className="rcBanner flex-center ">
                    <picture className="rcBanner-img-container animate__bounce infinite ">
                        <img src="https://i.pinimg.com/originals/72/3d/0a/723d0af616b1fe7d5c7e56a3532be3cd.png" alt="" />
                    </picture>
                    <div className="rcBanner-text ">Khel Bro <span className="rcBanner-text-bold">Win Real Cash!</span></div>
                    <div className="rcBanner-footer">For best experience, open&nbsp;<Link href="#!" style={{
                        color: 'rgb(44, 44, 44)',
                        fontWeight: 500, textDecoration: 'none'
                    }}>KhelBhai.com</Link>&nbsp;on&nbsp;<img src="https://khelbhai.in/images/global-chrome.png"
                        alt="" />&nbsp;chrome mobile</div>
                </div>
            </div>

        </>
    )
}

export default Register