import React, { useEffect, useState } from "react";
import "../css/layout.css";
import css from "../Modulecss/Home.module.css";
import findGif from "../css/loading.gif";
import axios from "axios";
import { Link, NavLink, useHistory, useLocation } from "react-router-dom";
import Header from "../Components/Header";
import socketIOClient from "socket.io-client";
import Swal from "sweetalert2";
import Rightcontainer from "../Components/Rightcontainer";


export default function Homepage() {
  const history = useHistory();

  const socket = socketIOClient("http://64.227.186.66:4000");
  // console.log(socket);

  /// user details start

  const [user, setUser] = useState();

  const role = async () => {
    const access_token = localStorage.getItem("token");
    const headers = {
      Authorization: `Bearer ${access_token}`,
    };
    await axios.get(`http://64.227.186.66:4000/me`, { headers }).then((res) => {
      setUser(res.data._id);
      console.log(res.data._id);
    });
  };

  /// user details end

  const [game_type, setGame_type] = useState(
    useLocation().pathname.split("/")[2]
  );
  const [Game_Ammount, setGame_Ammount] = useState();

  // console.log(game_type);

  const ChallengeCreate = (e) => {
    const access_token = localStorage.getItem("token");
    const headers = {
      Authorization: `Bearer ${access_token}`,
    };

    axios
      .post(
        "http://64.227.186.66:4000/challange/create",
        {
          Game_Ammount,
          Game_type: game_type,
        },
        { headers }
      )
      .then((res) => {
        if (res.data.msg === "you have already enrolled") {
          Swal.fire({
            title: "You have already enrolled",
            icon: "warning",
            confirmButtonText: "OK",
          });
        }
        if (res.data.msg === "Insufficient balance") {
          Swal.fire({
            title: "Insufficient balance",
            icon: "warning",
            confirmButtonText: "OK",
          });
        }
        if (
          res.data.msg ===
          "Game amount should be Greater then 50 and less then 10000"
        ) {
          Swal.fire({
            title: "Game amount should be Greater then 50 and less then 10000",
            icon: "warning",
            confirmButtonText: "OK",
          });
        } else {
          socket.emit("gameCreated", res.data);
        }
      })
      .catch((e) => {
        alert(e);
      });
  };

  const [allgame, setallgame] = useState();

  const [ALL, setALL] = useState();
  const [runningGames, setRunningGames] = useState();

  const Allgames = async () => {
    const access_token = localStorage.getItem("token");
    const headers = {
      Authorization: `Bearer ${access_token}`,
    };

    axios
      .get("http://64.227.186.66:4000/challange/all", { headers })
      .then((res) => {
        setallgame(res.data);
      });
  };

  const runningGame = async () => {
    const access_token = localStorage.getItem("token");
    const headers = {
      Authorization: `Bearer ${access_token}`,
    };

    axios
      .get("http://64.227.186.66:4000/challange/running/all", { headers })
      .then((res) => {
        setRunningGames(res.data);
        console.log(res.data);
      });
  };
  function winnAmount(gameAmount) {
    let profit = null;
    if (gameAmount >= 50 && gameAmount <= 250)
      profit = gameAmount * 10 / 100;
    else if (gameAmount > 250 && gameAmount <= 500)
      profit = 25;
    else if (gameAmount > 500)
      profit = gameAmount * 5 / 100;
    return gameAmount - profit;
  }

  useEffect(() => {
    socket.on("recieveGame", (game) => {
      Allgames();
    });
    socket.on("rejectedGame", (game) => {
      Allgames();
    });

    socket.on("challengeAccepted", (game) => {
      Allgames();
    });
    socket.on("ongoingChallenge", (game) => {
      Allgames();
      runningGame();
    });
    // runningGame()

    role();

    Allgames();
    runningGame();
  }, []);

  //accept Challange

  const AcceptChallang = (id) => {
    const access_token = localStorage.getItem("token");
    const headers = {
      Authorization: `Bearer ${access_token}`,
    };
    axios
      .put(
        `http://64.227.186.66:4000/challange/accept/${id}`,
        {
          Accepetd_By: headers,
          Acceptor_by_Creator_at: Date.now(),
        },
        {
          headers,
        }
      )
      .then((res) => {
        if (res.data.msg === "you have already enrolled") {
          Swal.fire({
            title: "You have already enrolled",
            icon: "warning",
            confirmButtonText: "OK",
          });
        }
        if (res.data.msg === "Insufficient balance") {
          Swal.fire({
            title: "Insufficient balance",
            icon: "warning",
            confirmButtonText: "OK",
          });
        } else {
          Allgames(res.data);
          socket.emit("acceptGame", res.data);
        }
      })
      .catch((e) => {
        alert(e);
      });
  };

  //reject Game
  const RejectGame = (id) => {
    const access_token = localStorage.getItem("token");
    const headers = {
      Authorization: `Bearer ${access_token}`,
    };

    axios
      .put(
        `http://64.227.186.66:4000/challange/reject/${id}`,
        {
          Accepetd_By: null,
          Status: "new",
          Acceptor_by_Creator_at: null,
        },
        { headers }
      )
      .then((res) => {
        socket.emit("gameRejected", res.data);
      })
      .catch((e) => {
        alert(e);
      });
  };

  //delete
  const deleteChallenge = (_id) => {
    const access_token = localStorage.getItem("token");
    const headers = {
      Authorization: `Bearer ${access_token}`,
    };

    axios
      .delete(`http://64.227.186.66:4000/challange/delete/${_id}`, { headers })
      .then((res) => {
        socket.emit("gameRejected", res.data);
      })
      .catch((e) => {
        alert(e);
      });
  };

  ///challange/running/update/

  const updateChallenge = (_id) => {
    const access_token = localStorage.getItem("token");
    const headers = {
      Authorization: `Bearer ${access_token}`,
    };

    axios
      .put(
        `http://64.227.186.66:4000/challange/running/update/${_id}`,
        {
          Acceptor_seen: true,
        },
        { headers }
      )
      .then((res) => {
        socket.emit("gameRejected", res.data);
      })
      .catch((e) => {
        alert(e);
      });
  };

  // const [roomCode, setRoomCode] = useState()
  let roomCode = null;
  let roomID = null;
  const getPost = async (path) => {
    socket.emit('roomCode', {game_id:path,status:'running'})

    const access_token = localStorage.getItem("token")
    const headers = {
      Authorization: `Bearer ${access_token}`
    }
    
    // await axios.get(`http://64.227.186.66:4000/room/get/${path}`, { headers })
    //   .then(async (response) => {
    //     // roomCode=response.data[1].vPrivateCode
    //     // roomID = response.data[1]._id,

    //     console.log(roomCode, roomID)
    //     await axios.patch(`http://64.227.186.66:4000/challange/roomcode/${path}`,
    //       {
    //         Room_code: response.data[1].vPrivateCode,
    //         Table_id: response.data[1]._id,
    //         Status: "running"
    //       }
    //       , { headers })
    //       .then((res) => {
    //         // setGame(res.data)
    //         socket.emit('challengeOngoing', res.data);
    //       })
    //       .catch((e) => alert(e))
    //   })





  }



  return (
    <>
      <div className="leftContainer">
        <Header />

        <div className={css.mainArea} style={{ paddingTop: "60px" }}>
          <span className={`${css.cxy} ${css.battleInputHeader}`}>
            Create a Battle!
          </span>

          <div className="mx-auto d-flex my-2 w-50">
            <div>
              <input
                className={css.formControl}
                type="tel"
                placeholder="Amount"
                onChange={(e) => setGame_Ammount(e.target.value)}
              />
            </div>

            <div className="set ml-1 ">
              {" "}
              <button
                className={`bg-green ${css.playButton} cxy m-1 position-static `}
                style={{ margin: "20px !important" }}
                onClick={(e) => {
                  e.preventDefault();
                  ChallengeCreate();
                }}
              >
                Set
              </button>
            </div>

            {/* <div className="set ml-1 ">  <button className={`bg-green ${css.playButton} cxy m-1 position-static `} style={{ margin: '20px !important' }} onClick={(e) => { e.preventDefault(); ChallengeCreate() }}>Set</button>
            </div> */}
          </div>
          <div className={css.dividerX}></div>

          <div className="px-4 py-3">
            <div className="mb-2">
              <img
                src={process.env.PUBLIC_URL + "/Images/Homepage/battleIcon.png"}
                alt=""
                width="20px"
              />
              <span className={`ml-2 ${css.gamesSectionTitle} `}>
                Open Battles
              </span>
              <span
                className={`${css.gamesSectionHeadline} text-uppercase position-absolute mt-2 font-weight-bold`}
                style={{ right: "1.5rem" }}
              >
                Rules
                <NavLink to="/Rules">
                <img
                  className="ml-2"
                  src={process.env.PUBLIC_URL + "/Images/Homepage/info.png"}
                  alt=""
                />
                </NavLink>
              </span>
            </div>

            {allgame &&
              allgame.map((allgame) => {
                if (
                  allgame.Status == "new" ||
                  (allgame.Status == "requested" &&
                    (user == allgame.Created_by._id ||
                      user == allgame.Accepetd_By._id)) ||
                  (allgame.Status == "running" &&
                    user == allgame.Accepetd_By._id)
                )
                  return (
                    <div className={`${css.betCard} mt-1`} key={allgame._id}>
                      <span
                        className={`${css.betCardTitle} pl-3 d-flex align-items-center text-uppercase`}
                      >
                        CHALLENGE FROM
                        <span className="ml-1" style={{ color: "brown" }}>
                          {allgame.Created_by.Name}
                        </span>
                        {user == allgame.Created_by._id &&
                          allgame.Status == "new" && (
                            <button
                              className={` p-1 m-1 mb-1 ml-auto btn-danger btn-sm`}
                              onClick={() => deleteChallenge(allgame._id)}
                            >
                              DELETE
                            </button>
                          )}
                      </span>
                      <div className="d-flex pl-3">
                        <div className="pr-3 pb-1">
                          <span className={css.betCardSubTitle}>Entry Fee</span>
                          <div>
                            <img
                              src={
                                process.env.PUBLIC_URL +
                                "/Images/LandingPage_img/global-rupeeIcon.png"
                              }
                              alt=""
                              width="21px"
                            />
                            <span className={css.betCardAmount}>
                              {allgame.Game_Ammount}
                            </span>
                          </div>
                        </div>
                        <div>
                          <span className={css.betCardSubTitle}>Prize</span>
                          <div>
                            <img
                              src={
                                process.env.PUBLIC_URL +
                                "/Images/LandingPage_img/global-rupeeIcon.png"
                              }
                              alt=""
                              width="21px"
                            />
                            <span className={css.betCardAmount}>
                              {allgame.Game_Ammount +
                                winnAmount(allgame.Game_Ammount)}
                            </span>
                          </div>
                        </div>
                        {user !== allgame.Created_by._id &&
                          allgame.Status == "new" && (
                            <button
                              className={`${css.bgSecondary} ${css.playButton} ${css.cxy}`}
                              onClick={() => AcceptChallang(allgame._id)}
                            >
                              Play
                            </button>
                          )}
                        {/* {user == allgame.Accepetd_By._id && allgame.Status == 'running' && <button className={`${css.bgSecondary} ${css.playButton} ${css.cxy}`} >start</button>} */}
                        {user == allgame.Created_by._id &&
                          allgame.Status == "new" && (
                            <div className="text-center col-5 ml-auto mt-auto mb-auto">
                              <div className="pl-2 text-center">
                                <img
                                  src={findGif}
                                  style={{ width: "15px", height: "15px" }}
                                />
                              </div>
                              <div style={{ lineHeight: 1 }}>
                                <span className={css.betCard_playerName}>
                                  Finding Player!
                                </span>
                              </div>
                            </div>
                          )}
                        {user !== allgame.Created_by._id &&
                          allgame.Status == "requested" && (
                            <div className="d-flex ml-auto align-items-center">
                              <Link
                                className={`${css.bgSecondary} ${css.playButton} ${css.cxy} position-relative mx-1 text-white`}
                              >
                                requested
                              </Link>
                              <button
                                className={`${css.bgSecondary} ${css.playButton} ${css.cxy} position-relative mx-1 bg-danger`}
                                onClick={() => RejectGame(allgame._id)}
                              >
                                cancel
                              </button>
                            </div>
                          )}
                        {user !== allgame.Created_by._id &&
                          allgame.Status == "running" && (
                            <Link
                              className={`${css.bgSecondary} ${css.playButton} ${css.cxy} bg-success`}
                              to={`/viewgame1/${allgame._id}`}
                              onClick={(e) => updateChallenge(allgame._id)}
                            >
                              start
                            </Link>
                          )}
                        {user == allgame.Created_by._id &&
                          allgame.Status == "requested" && (
                            <div className="d-flex ml-auto align-items-center">
                              <Link
                                className={`${css.bgSecondary} ${css.playButton} ${css.cxy} position-relative mx-1 bg-success`}
                                to={`/viewgame1/${allgame._id}`}
                                onClick={(e) => getPost(allgame._id)}
                              >
                                start
                              </Link>
                              <button
                                className={`${css.bgSecondary} ${css.playButton} ${css.cxy} position-relative mx-1 bg-danger`}
                                onClick={() => RejectGame(allgame._id)}
                              >
                                reject
                              </button>
                              <div className="text-center col">
                                <div className="pl-2">
                                  <img
                                    src={process.env.PUBLIC_URL + '/Images/Homepage/Avatar4.png'}
                                    alt=""
                                    width="35px"
                                  />
                                </div>
                                <div style={{ lineHeight: 1 }}>
                                  <span className={css.betCard_playerName}>
                                    {allgame.Accepetd_By.Name}
                                  </span>
                                </div>
                              </div>
                            </div>
                          )}
                      </div>
                    </div>
                  );
              })}
          </div>
          <div className={css.dividerX}></div>
          <div className="px-4 py-3">
            <div className="mb-2">
              <img
                src={
                  process.env.PUBLIC_URL +
                  "/Images/LandingPage_img/global-rupeeIcon.png"
                }
                alt=""
                width="20px"
              />
              <span className={`ml-2 ${css.gamesSectionTitle} `}>
                Running Battles
              </span>
            </div>
            {runningGames &&
              runningGames.map((runnig) => {
                if (
                  user !== runnig.Accepetd_By._id ||
                  (user == runnig.Accepetd_By._id &&
                    runnig.Acceptor_seen == true) ||
                  (user == runnig.Accepetd_By._id && runnig.Status == "pending")
                )
                  return (
                    <div id="" className={`${css.betCard} mt-1`}>
                      <div className="d-flex">
                        <span
                          className={`${css.betCardTitle} pl-3 d-flex align-items-center text-uppercase`}
                        >
                          PLAYING FOR
                          <img
                            className="mx-1"
                            src={
                              process.env.PUBLIC_URL +
                              "/Images/LandingPage_img/global-rupeeIcon.png"
                            }
                            alt=""
                            width="21px"
                          />
                          {runnig.Game_Ammount}
                        </span>
                        {(user == runnig.Accepetd_By._id ||
                          user == runnig.Created_by._id) && (
                          <Link
                            className={`${css.bgSecondary} ${css.playButton} ${css.cxy} position-relative m-2 mx-1 bg-success`}
                            style={{
                              right: "0px",
                              top: "-6px",
                              padding: "10px 17px",
                            }}
                            to={`/viewgame1/${runnig._id}`}
                          >
                            view
                          </Link>
                        )}
                        <div
                          className={`${css.betCardTitle} d-flex align-items-center text-uppercase`}
                        >
                          <span className="ml-auto mr-3">
                            PRIZE
                            <img
                              className="mx-1"
                              src="https://khelbhai.in/images/global-rupeeIcon.png"
                              alt=""
                              width="21px"
                            />
                            {runnig.Game_Ammount +
                              winnAmount(runnig.Game_Ammount)}
                          </span>
                        </div>
                      </div>
                      <div className="py-1 row">
                        <div className="pr-3 text-center col-5">
                          <div className="pl-2">
                            <img
                              src={process.env.PUBLIC_URL + '/Images/Homepage/Avatar4.png'}
                              alt=""
                              width="40px"
                            />
                          </div>
                          <div style={{ lineHeight: 1 }}>
                            <span className={css.betCard_playerName}>
                              {runnig.Created_by.Name}
                            </span>
                          </div>
                        </div>
                        <div className="pr-3 text-center col-2 cxy">
                          <div>
                            <img
                              src={process.env.PUBLIC_URL + '/Images/Homepage/versus.png'}
                              alt=""
                              width="21px"
                            />
                          </div>
                        </div>
                        <div className="text-center col-5">
                          <div className="pl-2">
                            <img
                              src={process.env.PUBLIC_URL + '/Images/Homepage/Avatar4.png'}
                              alt=""
                              width="35px"
                            />
                          </div>
                          <div style={{ lineHeight: 1 }}>
                            <span className={css.betCard_playerName}>
                              {runnig.Accepetd_By.Name}
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  );
              })}
          </div>
        </div>
      </div>
      <div className="rightContainer">
        <Rightcontainer />
      </div>
    </>
  );
}
