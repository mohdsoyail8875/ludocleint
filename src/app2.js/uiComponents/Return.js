import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useLocation } from 'react-router-dom';

function Return() {

    // const location = useLocation();
    // const path = location.path.split("/")[2];
    var url_string = window.location.href; //window.location.href
    var url = new URL(url_string);
    var order_id = url.searchParams.get("order_id");
    var order_token = url.searchParams.get("order_token");
    const [status, setStatus] = useState()
    useEffect(() => {

        const access_token = localStorage.getItem("token")
        const headers = {
            Authorization: `Bearer ${access_token}`
        }
        axios.post(`http://64.227.186.66:4000/deposit/response`, { order_id, order_token }, { headers })
            .then((res) => {
                setStatus(res.data.status);

                // window.location.reload()

            })
    }, [])

    return (
        <div>
            {status && <div>{status}</div>}
        </div>
    )
}

export default Return