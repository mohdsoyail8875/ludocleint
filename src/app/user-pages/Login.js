import React, { Component, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Form } from 'react-bootstrap';
import axios from 'axios';

const Login = () => {
  const history = useHistory()


  const [Phone, setPhone] = useState();
  const [twofactor_code, settwofactor_code] = useState();
  const [otp, setOtp] = useState(false)

  // console.log(FirstName);
  const handleClick = async (e) => {
    e.preventDefault();
    if (!Phone) {
      alert("Enter Phone and Password")
    } else {
      const { data } = await axios.post("http://64.227.186.66:4000/login", {
        Phone,
      }).then((respone) => {

        setOtp(true)
        console.log(respone.data);
      });
    }
  }


  const varifyOtp = async (e) => {
    e.preventDefault();
    if (!Phone) {
      alert("Enter Phone and Password")
    } else {
      const { data } = await axios.post("http://64.227.186.66:4000/login/finish", {
        Phone,
        twofactor_code
      }).then((respone) => {
        const token = respone.data.data
        localStorage.setItem("token", token)
        history.push("/dashboard")
        window.location.reload(true)
        // console.log(respone.data);
      });
    }
  }

  return (
    <div>
      <div className="d-flex align-items-center auth px-0">
        <div className="row w-100 mx-0">
          <div className="col-lg-4 mx-auto">
            <div className="card text-left py-5 px-4 px-sm-5">
              <div className="brand-logo">
                <img src={require("../../assets/images/logo.svg")} alt="logo" />
              </div>
              <h4>Hello! let's get started</h4>
              <h6 className="font-weight-light">Sign in to continue.</h6>
              <Form className="pt-3">
                {!otp && <Form.Group className="d-flex search-field">
                  <Form.Control type="email" placeholder="Username" size="lg" className="h-auto"

                    onChange={(e) => setPhone(e.target.value)} />
                </Form.Group>}
                {otp && <Form.Group className="d-flex search-field">
                  <Form.Control type="password" placeholder="Password" size="lg" className="h-auto" onChange={(e) => settwofactor_code(e.target.value)} />
                </Form.Group>}
                <div className="mt-3">
                  {!otp && <button button className="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" onClick={(e) => handleClick(e)}>Otp</button>}
                  {otp && <button className="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" onClick={(e) => varifyOtp(e)}></button>}
                </div>
                <div className="my-2 d-flex justify-content-between align-items-center">
                  <div className="form-check">
                    <label className="form-check-label text-muted">
                      <input type="checkbox" className="form-check-input" />
                      <i className="input-helper"></i>
                      Keep me signed in
                    </label>
                  </div>
                  <a href="!#" onClick={event => event.preventDefault()} className="auth-link text-muted">Forgot password?</a>
                </div>
                <div className="mb-2">
                  <button type="button" className="btn btn-block btn-facebook auth-form-btn">
                    <i className="mdi mdi-facebook mr-2"></i>Connect using facebook
                  </button>
                </div>
                <div className="text-center mt-4 font-weight-light">
                  Don't have an account? <Link to="/user-pages/register" className="text-primary">Create</Link>
                </div>
              </Form>
            </div>
          </div>
        </div>
      </div >
    </div >
  )
}


export default Login
