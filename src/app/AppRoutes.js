import React, { Component, Suspense, lazy } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Spinner from '../app/shared/Spinner';
import App2 from '../app2.js/App2';
import Home from '../app2.js/Components/Home';
import Aadhar from '../app2.js/uiComponents/Aadhar';
import Addcase from '../app2.js/uiComponents/Addcase';
import Addfunds from '../app2.js/uiComponents/Addfunds';
import Deposite from '../app2.js/uiComponents/Deposit';
import Games from '../app2.js/uiComponents/Games';
import Help from '../app2.js/uiComponents/Help';
import Homepage from '../app2.js/uiComponents/Homepage';
import Landing from '../app2.js/uiComponents/Landing';
import Mywallet from '../app2.js/uiComponents/Mywallet';
import Notification from '../app2.js/uiComponents/Notification';
import Notify from '../app2.js/uiComponents/Notify';
import Pan from '../app2.js/uiComponents/Pan';
import Profile from '../app2.js/uiComponents/Profile';
import Refer from '../app2.js/uiComponents/Refer';
import Referralhis from '../app2.js/uiComponents/Referralhis';
import Return from '../app2.js/uiComponents/Return';
import Support from '../app2.js/uiComponents/Support';
import Transactionhistory from '../app2.js/uiComponents/Transactionhistory';
import ViewGame1 from '../app2.js/uiComponents/ViewGame1';
import Chat from '../app2.js/uiComponents/Web';
import Withdrawopt from '../app2.js/uiComponents/Withdrawopt';
import Views from './components/challengeManagement/Views';
import deposits from './components/transaction/Deposits';
import UserKyc from './components/userManagement/UserKyc';
import ViewKyc from './components/userManagement/ViewKyc';
import Deposithistory from './components/transaction/Deposithistory';
import Withdrawlhistory from './components/transaction/Withdrawlhistory';

import Earings from './dashboard/Earings';

// Admin Management start
const Alladmin = lazy(() => import('./components/adminManagement/Alladmin'));
const Addadmin = lazy(() => import('./components/adminManagement/Addadmin'));
// Admin Management end

// Game Management start
const Allgames = lazy(() => import('./components/gameManagement/Allgames'));
const Addgame = lazy(() => import('./components/gameManagement/Addgame'));
// Game Management end

// User Management start
const Allusers = lazy(() => import('./components/userManagement/Allusers'));
const Adduser = lazy(() => import('./components/userManagement/Adduser'));
// User Management end

// Agent Management start
const Allagents = lazy(() => import('./components/agentManagement/Allagent'));
const Addagent = lazy(() => import('./components/agentManagement/Addagent'));
// User Management end

// Challenge Management start
const Allchallenges = lazy(() => import('./components/challengeManagement/Allchallenge'));
// Challenge Management end

// Transaction Management start
const Alldeposits = lazy(() => import('./components/transaction/Deposits'));
const Allwithdrawl = lazy(() => import('./components/transaction/Withdrawl'));
const Penaltybonus = lazy(() => import('./components/transaction/Penalty&bonus'));
// Transaction Management end

const Dashboard = lazy(() => import('./dashboard/Dashboard'));

const Buttons = lazy(() => import('./basic-ui/Buttons'));
const Dropdowns = lazy(() => import('./basic-ui/Dropdowns'));
const Typography = lazy(() => import('./basic-ui/Typography'));

const BasicElements = lazy(() => import('./form-elements/BasicElements'));

const BasicTable = lazy(() => import('./tables/BasicTable'));

const Mdi = lazy(() => import('./icons/Mdi'));

const ChartJs = lazy(() => import('./charts/ChartJs'));

const Error404 = lazy(() => import('./error-pages/Error404'));
const Error500 = lazy(() => import('./error-pages/Error500'));

const Login = lazy(() => import('./user-pages/Login'));
const Register1 = lazy(() => import('./user-pages/Register'));
const Login1 = lazy(() => import('../app2.js/uiComponents/Login'));
const Register11 = lazy(() => import('../app2.js/uiComponents/Register'));

// const Homepage = lazy(() => import('../app2.js/uiComponents/Homepage'));




class AppRoutes extends Component {
  render() {
    return (
      <Suspense fallback={<Spinner />}>
        <Switch>
          <Route exact path="/dashboard" component={Dashboard} />
          <Route path="/dashboard" component={Dashboard} />

          <Route path="/earinigs" component={Earings} />

          {/* Admin Routes start */}

          <Route path="/admin/alladmins" component={Alladmin} />
          <Route path="/admin/addadmin" component={Addadmin} />

          {/* Admin Routes end */}


          {/* Game Routes start */}

          <Route path="/game/allgames" component={Allgames} />
          <Route path="/game/addgame" component={Addgame} />

          {/* Game Routes end */}


          {/* user Routes start */}

          <Route path="/user/allusers" component={Allusers} />

          <Route path="/user/adduser/:id" component={Adduser} />



          <Route path="/user/adduser" component={Adduser} />

          <Route path="/user/UserKyc" component={UserKyc} />
          <Route path="/user/view" component={ViewKyc} />

          {/* user Routes end */}


          {/* Agent Routes start */}

          <Route path="/agent/allagents" component={Allagents} />
          <Route path="/agent/addagent" component={Addagent} />

          {/* Agent Routes end */}


          {/* challenge Routes start */}

          <Route path="/challenge/allchallenges" component={Allchallenges} />

          {/* challenge Routes end */}


          {/* Transaction Routes start */}

          <Route path="/transaction/alldeposits" component={Alldeposits} />
          <Route path="/transaction/allwithdrawl" component={Allwithdrawl} />
          <Route path="/transaction/penaltybonus" component={Penaltybonus} />
          <Route path="/transaction/deposit_history" component={Deposithistory}/>
          <Route path="/transaction/withdraw_history" component={Withdrawlhistory}/>

          {/* Transaction Routes end */}

          <Route path="/basic-ui/buttons" component={Buttons} />
          <Route path="/basic-ui/dropdowns" component={Dropdowns} />
          <Route path="/basic-ui/typography" component={Typography} />

          <Route path="/form-Elements/basic-elements" component={BasicElements} />

          <Route path="/tables/basic-table" component={BasicTable} />

          <Route path="/icons/mdi" component={Mdi} />

          <Route path="/charts/chart-js" component={ChartJs} />


          <Route path="/user-pages/login-1" component={Login} />
          <Route path="/user-pages/register-1" component={Register1} />

          <Route path="/error-pages/error-404" component={Error404} />
          <Route path="/error-pages/error-500" component={Error500} />
          <Route exact path="/" component={App2} />
          <Route path="/login" component={Login1} />
          <Route path="/register" component={Register11} />
          <Route path="/Homepage" component={Homepage} />

          <Route path="/adminlogin" component={Login} />

          <Route path="/profile" component={Profile} />
          <Route path="/help" component={Help} />
          <Route path="/home" component={Home} />


          <Route path="/Deposit" component={Deposite} />
          <Route path="/KYC/update-pan" component={Pan} />
          <Route path="/KYC/update-aadhar" component={Aadhar} />
          <Route path="/Referral-history" component={Referralhis} />
          <Route path="/landing/:id" component={Landing} />
          <Route path="/viewgame1/:id" component={ViewGame1} />
          <Route path="/Games" component={Games} />
          <Route path="/view/:id" component={Views} />
          <Route path="/Addcase" component={Addcase} />
          <Route path="/Addfunds" component={Addfunds} />
          <Route path="/Withdrawopt" component={Withdrawopt} />
          <Route path="/Wallet" component={Mywallet} />
          <Route path="/Support" component={Support} />
          <Route path="/Notification" component={Notification} />
          <Route path="/refer" component={Refer} />
          <Route path="/transaction" component={Transactionhistory} />
          <Route path="/transaction-history" component={Transactionhistory} />
          <Route path="/return" component={Return} />
          <Route path="/notify" component={Notify} />
          <Route path="/web" component={Chat} />
          <Route path="/redeem/refer" component={Refer} />
         
          
          {/* <Redirect to="/dashboard" /> */}
        </Switch>
      </Suspense>
    );
  }
}

export default AppRoutes;