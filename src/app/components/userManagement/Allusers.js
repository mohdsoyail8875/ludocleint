import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

export default function Allusers() {

    const [user, setUser] = useState(false)

    const getUser = async () => {
        const access_token = localStorage.getItem("token")
        const headers = {
            Authorization: `Bearer ${access_token}`
        }
        const data = await axios.get(`http://64.227.186.66:4000/User/all`, { headers })
            .then((res) => {
                setUser(res.data)
            }).catch((e) => alert(e))
    }

    const blockPlayer = (player) => {
        const confirmBox = window.confirm(`are you sure block ${player.Name}`);
        if (confirmBox === true) {
            const access_token = localStorage.getItem("token")
            const headers = {
                Authorization: `Bearer ${access_token}`
            }
            const userType = player.user_type == 'Block' ? 'Unblock' : 'Block';
            axios.patch(`http://64.227.186.66:4000/user/edit/${player._id}`, { user_type: userType }, { headers })
                .then((res) => {
                    getUser(res.data)
                    console.log(res.data);
                })
        }

    }

    useEffect(() => {
        getUser()
    }, [])




    return (
        user && <div>
            <h4 className='font-weight-bold my-3'>All Game Type LIST</h4>
            <div className="row ">
                <div className="col-12 grid-margin">
                    <div className="card">
                        <div className="card-body">
                            <h4 className="card-title">Order Status</h4>
                            <div className="table-responsive">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th> ID</th>
                                            <th> Name</th>
                                            <th> LKID</th>
                                            <th> Mobile </th>
                                            <th> Balance </th>
                                            <th> Reffered By </th>
                                            <th> Verified </th>
                                            <th> Action </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {user.map((user, key) => (
                                            <tr key={user._id}>
                                                <td>{key + 1}</td>
                                                <td>{user._id}</td>
                                                <td>
                                                    <span className="pl-2 text-primary">{user.Name}</span>
                                                </td>
                                                <td>{user.LKID}</td>
                                                <td className='text-success'>{user.Phone}</td>
                                                <td>₹{user.Wallet_balance}</td>
                                                <td></td>
                                                <td>
                                                    <div className="badge badge-outline-success">verified</div>
                                                </td>
                                                <td>
                                                    <button type='button' className={`btn  mx-1 ${user.user_type == 'Block' ? 'btn-success' : 'btn-danger'}`} onClick={() => { blockPlayer(user) }}>{
                                                        user.user_type == "Block" ? "Unblock" : "Block"
                                                    }</button>
                                                    <Link type='button' className="btn btn-info mx-1" to={`/user/adduser/${user._id}`}>Edit User</Link>
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
