import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useHistory, useLocation,Link } from 'react-router-dom'

export default function UserKyc () {
    const DemoKyc=[
        {
            id: 'aftghsj',
            name: "User1",
            phone: "1234567890",
            DocumentType: "Aadhar",
            Document: "https://5.imimg.com/data5/YU/NL/VK/ANDROID-65691403/product-jpeg-500x500.jpeg",
            KYCStatusDate: "2020-06-01",
            KYCVerified: "Yes",            
            KYCVerifiedOn: "",
        },
        {
            id: 'aftghsj',
            name: "User2",
            phone: "1234567890",
            DocumentType: "Voter ID",
            Document: "https://5.imimg.com/data5/YU/NL/VK/ANDROID-65691403/product-jpeg-500x500.jpeg",
            KYCStatusDate: "2020-06-05",
            KYCVerified: "No",
            KYCVerifiedOn: "",
        },
        {
            id: 'aftghsj',
            name: "User3",
            phone: "1234567890",
            DocumentType: "Pan Card",
            Document: "https://5.imimg.com/data5/YU/NL/VK/ANDROID-65691403/product-jpeg-500x500.jpeg",
            KYCStatusDate: "2020-06-10",
            KYCVerified: "Yes",
            KYCVerifiedOn: "",
        },
        {
            id: 'aftghsj',
            name: "User4",
            phone: "1234567890",
            DocumentType: "Aadhar",
            Document: "https://5.imimg.com/data5/YU/NL/VK/ANDROID-65691403/product-jpeg-500x500.jpeg",
            KYCStatusDate: "2020-06-15",
            KYCVerified: "No",
            KYCVerifiedOn: "",
        }            
    ]  

        return (
    <>
      {/* <h4 className='font-weight-bold my-3'>ALL CHALLANGES</h4> */}
      <div className="row ">
        <div className="col-12 grid-margin">
          <div className="card">
            <div className="card-body">
              <h4 className="card-title">KYC</h4>
              <div className="table-responsive">
                <table className="table">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th> ID</th>
                      <th> Name</th>
                      <th> Phone</th>
                      <th> Document Type</th>
                      <th> Document</th>
                      <th> KYC REQUEST Date </th>
                      <th> Approve or Reject </th>
                      <th> View </th>
                    </tr>
                  </thead>
                    <tbody>
                        {DemoKyc.map((item,index)=>{
                            return(
                                <tr key={index}>
                                    <td>{index+1}</td>
                                    <td>{item.id}</td>
                                    <td>{item.name}</td>
                                    <td>{item.phone}</td>
                                    <td>{item.DocumentType}</td>
                                    <td><img src={item.Document} alt="kyc" style={{
                                      borderRadius: '5px',
                                      width: '4rem',
                                      height: '4rem',  
                                    }}
                                    id={`kyc${index}`}
                                    onClick={()=>{
                                      // window.open(item.Document)
                                      console.log(item.Document)
                                      const kyc = document.getElementById(`kyc${index}`)
                                      const width = kyc.style.width
                                      const height = kyc.style.height
                                      if(width === '4rem'){
                                        kyc.style.width = '100%'
                                        kyc.style.height = '100%'
                                      }
                                      else{
                                        kyc.style.width = '4rem'
                                        kyc.style.height = '4rem'
                                      }
                                  }}
                                    /></td>
                                    <td>{item.KYCStatusDate}</td>
                                    <td>
                                        <button className="btn btn-success mr-1">Approve</button>
                                        <button className="btn btn-danger">Reject</button>
                                    </td>
                                    <td><Link to="/user/view" className="btn btn-primary">View</Link></td>
                                </tr>
                            )
                        }
                        )}
                    </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
