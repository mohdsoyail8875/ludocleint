import axios from 'axios'
import React, { useEffect, useState } from 'react'

export default function Allgames() {

    const [game, setGame] = useState()


    const loadGame = () => {
        const access_token = localStorage.getItem('token')

        const headers = {
            Authorization: `Bearer ${access_token}`
        }
        axios.get(`http://64.227.186.66:4000/game/type`, { headers })
            .then((res) => {
                setGame(res.data)
            })
    }
    const deleteRecord = (_id) => {
        const confirmBox = window.confirm(
            `are you sure ${game.Game}`
        )
        if (confirmBox === true) {
            const access_token = localStorage.getItem("token")

            const headers = {
                Authorization: `Bearer ${access_token}`
            }
            axios.delete(`http://64.227.186.66:4000/game/delete/${_id}`, { headers })
                .then((result) => {
                    loadGame();
                })
                .catch(() => {
                    alert('Error in the Code');
                });
        }
    };


    useEffect(() => {
        loadGame()


    }, [])




    if (game === undefined) {
        return null
    }
    return (
        <>
            <h4 className='font-weight-bold my-3'>All Game Type LIST</h4>
            <div className="row ">
                <div className="col-12 grid-margin">
                    <div className="card">
                        <div className="card-body">
                            <h4 className="card-title">Order Status</h4>
                            <div className="table-responsive">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th> ID</th>
                                            <th> Game</th>
                                            <th> Action </th>
                                        </tr>
                                    </thead>
                                    {game.map((game, key) => (


                                        <tbody key={game._id}>
                                            <tr>
                                                <td>{key + 1}</td>
                                                <td>{game._id}</td>
                                                <td>
                                                    <span className="pl-2">{game.Game}</span>
                                                </td>
                                                <td>
                                                    <div className="badge badge-outline-danger" onClick={() => {

                                                        deleteRecord(game._id)

                                                    }}>delete</div>
                                                </td>
                                            </tr>

                                        </tbody>
                                    ))}
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
