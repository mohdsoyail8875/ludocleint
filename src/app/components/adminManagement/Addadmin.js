import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'

export default function Addadmin() {

    const history = useHistory()
    const [Name, setName] = useState()
    const [Phone, setPhone] = useState()
    const [Password, setPassword] = useState()
    const [cPassword, setCPassword] = useState()



    const addAdmin = async (e) => {

        e.preventDefault();

        if (Password !== cPassword) {
            alert("Passwords don't match");
        } else {
            const data = await axios.post("http://64.227.186.66:4000/admin/register", {
                Name,
                Phone,
                Password,
            }).then((res) => {

                history.push("/admin/alladmins")
            })
        }
    }

    return (
        <div>
            <h4 className='text-uppercase font-weight-bold my-3'>Add New User</h4>

            <form id="add_admin_form" action="" method="post">
                <div className="form-row">
                    <div className="form-group col-md-4">
                        <label htmlFor="name">Name</label>
                        <input type="text" className="form-control" id="name" name="name" placeholder="Name" onChange={(e) => {
                            setName(e.target.value)
                        }} />
                    </div>
                    <div className="form-group col-md-4">
                        <label htmlFor="mobile">Mobile</label>
                        <input type="text" className="form-control form-control" maxLength={10} id="mobile" name="mobile" placeholder="MObile" onChange={(e) => setPhone(e.target.value)} />
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-group col-md-4">
                        <label htmlFor="passowrd">Password</label>
                        <input type="password" className="form-control" id="password" name="password" placeholder="Passowrd" onChange={(e) => {
                            setPassword(e.target.value)
                        }} />
                    </div>
                    <div className="form-group col-md-4">
                        <label htmlFor="mobile">Confirm Password</label>
                        <input type="password" className="form-control" id="confirm_password" name="confirm_password" placeholder="Confirm Passowrd" onChange={(e) => setCPassword(e.target.value)} />
                    </div>
                    <div className="form-group col-md-8">
                        <button type="submit" className="btn btn-success float-right" onClick={(e) => addAdmin(e)}>ADD ADMIN</button>
                    </div>
                </div>

            </form>

        </div>
    )
}
