import axios from 'axios'
import React, { useEffect, useState } from 'react'

export default function Alladmin() {

    const [Admin, setAdmin] = useState()

    useEffect(() => {
        const access_token = localStorage.getItem('token')
        const headers = {
            Authorization: `Bearer ${access_token}`
        }
        axios.get(`http://64.227.186.66:4000/admin/all`, { headers })
            .then((res) => {
                setAdmin(res.data)
                console.log(res.data);
            })
            .catch((e)=>{
                console.log(e);
            })
    },[])

    if (Admin === undefined) {
        return null
    }

    return (
        <>
        
            <h4 className='text-uppercase font-weight-bold my-3'>all user list</h4>
            <div className="row ">
                <div className="col-12 grid-margin">
                    <div className="card">
                        <div className="card-body">
                            <h4 className="card-title">Order Status</h4>
                            <div className="table-responsive">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th> ID</th>
                                            <th> Name</th>
                                            <th> Mobile </th>
                                            <th> Verified </th>
                                        </tr>
                                    </thead>
                                    {Admin.map((admin,key) => (

                                        <tbody key={admin._id} >
                                            <tr>
                                                <td>{key+1}</td>
                                                <td>{admin._id}</td>
                                                <td>
                                                    <span className="pl-2">{admin.Name}</span>
                                                </td>
                                                <td>{admin.Phone}</td>
                                                <td>
                                                    <div className="badge badge-outline-success">Verified</div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    ))}
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
