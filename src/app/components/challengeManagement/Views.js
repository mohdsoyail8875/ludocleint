import React, { useEffect, useState } from 'react'

import axios from 'axios'
import { useLocation } from 'react-router-dom'





const styles = {
    color: '#fff !important',
}


function Views() {

    const location = useLocation();
    const path = location.pathname.split("/")[2];

    const [game, setall] = useState()
    const [winner, setWinner] = useState(null)
    const Allchallenge = () => {

        const access_token = localStorage.getItem('token')
        const headers = {
            Authorization: `Bearer ${access_token}`
        }
        axios.get(`http://64.227.186.66:4000/challange/${path}`, { headers })
            .then((res) => {
                setall(res.data)
                // console.log(res.data);
            }).catch((e) => {
                alert(e)
            })

    }
    // Allchallenge()



    const CancelGame = async (id) => {
        const confirm = window.confirm("are you sure to cancel")

        if (confirm == true) {
            const access_token = localStorage.getItem('token')
            const headers = {
                Authorization: `Bearer ${access_token}`

            }
            axios.patch(`http://64.227.186.66:4000/challange/Cancel/admin/${path}`, { Cancelled_by: access_token }, { headers })
                .then((res) => {
                    Allchallenge()
                })
        }
        else {
            window.alert("sorry try again")
        }
    }



    const updateadmin = async (id) => {
        console.log(winner);
        const confirm = window.confirm("are you sure to update")

        if (confirm == true) {
            const access_token = localStorage.getItem('token')
            const headers = {
                Authorization: `Bearer ${access_token}`

            }
            axios.post(`http://64.227.186.66:4000/challange/admin/result/${path}`, {
                winner,
                Status_Update_By: access_token
            }, { headers })
                .then((res) => {
                    Allchallenge()
                })
        }
        else {
            window.alert("sorry try again")
        }
    }

    useEffect(() => {
        Allchallenge()
    }, [])

    if (game == undefined) {
        return null
    }

    return (
        <div>
            <div>
                <h3 className="font-weight-bold fs-40 fw-800" style={{ fontSize: '2rem' }}>Challenge Details</h3>
            </div>


            <div>
                <div className="card" style={{
                    fontSize: '18px',
                    margin: '5px',
                    lineHeight: '1.8'

                }}>
                    <div className='d-flex '>
                        <span>Challenge id:</span>
                        <span className="font-weight-bold">{game._id}</span>
                    </div>

                    <div className='d-flex'>
                        <span>Game amount:</span>
                        <span className="font-weight-bold">{game.Game_Ammount}</span>
                    </div>

                    <div className='d-flex'>
                        <span>Game type:</span>
                        <span className=" font-weight-bold badge bg-warning">{game.Game_type}</span>
                    </div>

                    <div className='d-flex'>
                        <span>Challenge Set time:</span>
                        <span className="font-weight-bold ">{game.createdAt}</span>
                    </div>

                    <div className='d-flex'>
                        <span>Challenge Request time:</span>
                        <span className="font-weight-bold">{game.createdAt}</span>
                    </div>

                    <div className='d-flex'>
                        <span>Challenge Accept time:</span>
                        <span className="font-weight-bold">{game.createdAt}</span>
                    </div>

                    <div className='d-flex'>
                        <span>Room code share time:</span>
                        <span className="font-weight-bold">{game.createdAt}</span>
                    </div>

                    <div className='d-flex'>
                        <span>Room code:</span>
                        <span className="font-weight-bold">{game.Room_code}</span>
                    </div>

                    <div className='d-flex'>
                        <span>Game status:</span>
                        <span className="font-weight-bold badge bg-warning">{game.Status}</span>
                    </div>

                    <hr />

                    <div className='d-flex'>
                        <span>Set by: </span>
                        <span>{game.Created_by.Name}</span>
                    </div>
                    {/* 
                    <div className='d-flex'>
                        <span>Ludoking Id:</span>
                        <span>123123</span>
                    </div> */}
                    <hr />
                    <div className='d-flex'>
                        <span>Accepted by:</span>
                        <span>{game.Accepetd_By.Name}</span>
                    </div>

                    {/* <div className='d-flex'>
                        <span>Ludoking Id:</span>
                        <span>123123</span>
                    </div> */}

                    <div className='d-flex'>
                        <span>Status update time:</span>
                        <span className="font-weight-bold">{game.updatedAt}</span>
                    </div>

                    <div className='d-flex'>
                        <span>Status :</span>
                        <span className="font-weight-bold">Win</span>
                    </div>

                    <hr />
                    {/* <span className="font-weight-bold">Winner:{game.Winner ? game.Winner : null}</span> */}

                    {(game.Status == "pending" || game.Status == "conflict") && <div className="d-flex" >
                        <div style={{ padding: '10px' }}>
                            <span>Select winner</span>
                        </div>
                        <div style={{ padding: '10px' }}>
                            <select class="form-select" aria-label="Default select example" onChange={(e) => setWinner(e.target.value)}>
                                <option selected> select winner</option>
                                <option value={game.Created_by._id}>{game.Created_by.Name}</option>
                                <option value={game.Accepetd_By._id}>{game.Accepetd_By.Name}</option>

                            </select>
                        </div>
                        <div style={{ padding: '10px' }}>
                            <button type="button" className="btn btn-primary" onClick={updateadmin}>update winner</button>
                        </div>
                    </div>}
                    <div>
                        {game.Status == "completed" && game.Status == "cancelled" && <button type="button" className="btn btn-danger" style={styles} onClick={CancelGame} >Cancel Challenge</button>}
                    </div>
                </div>


                <div></div>
            </div>


        </div>
    )
}

export default Views








