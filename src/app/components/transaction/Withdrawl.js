import React from 'react'

export default function withdrawl() {
  return (
    <>
    {/* <h4 className='font-weight-bold my-3'>ALL CHALLANGES</h4> */}
    <div className="row ">
        <div className="col-12 grid-margin">
            <div className="card">
                <div className="card-body">
                    <h4 className="card-title">ALL WITHDRAWL REQUEST</h4>
                    <div className="table-responsive">
                        <table className="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th> ID</th>
                                    <th> User</th>
                                    <th> Amount</th>
                                    <th> UPI APP </th>
                                    <th> UPI ID </th>
                                    <th> Time</th>
                                    <th> Status </th>
                                    <th> Action </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>458</td>
                                    <td>
                                        <span className="pl-2 "><span className='text-primary'>KGF</span>(359)</span>
                                    </td>
                                    <td>500</td>
                                    <td>
                                        <span className="pl-2">PhonePe</span>
                                    </td>
                                    <td>05165489418</td>
                                    <td>2022-04-22 08:50:32</td>
                                    <td className='text-primary font-weight-bold'>New</td>
                                    <td>
                                        <button type='button' className="btn btn-danger mx-1">Update</button>
                                        <button type='button' className="btn btn-danger mx-1">Status</button>
                                        <button type='button' className="btn btn-danger mx-1">Cancel</button>
                                        <button type='button' className="btn btn-danger mx-1">Request</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>458</td>
                                    <td>
                                        <span className="pl-2 "><span className='text-primary'>KGF</span>(359)</span>
                                    </td>
                                    <td>500</td>
                                    <td>
                                        <span className="pl-2">PhonePe</span>
                                    </td>
                                    <td>05165489418</td>
                                    <td>2022-04-22 08:50:32</td>
                                    <td className='text-primary font-weight-bold'>New</td>
                                    <td>
                                        <button type='button' className="btn btn-danger mx-1">Update</button>
                                        <button type='button' className="btn btn-danger mx-1">Status</button>
                                        <button type='button' className="btn btn-danger mx-1">Cancel</button>
                                        <button type='button' className="btn btn-danger mx-1">Request</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>458</td>
                                    <td>
                                        <span className="pl-2 "><span className='text-primary'>KGF</span>(359)</span>
                                    </td>
                                    <td>500</td>
                                    <td>
                                        <span className="pl-2">PhonePe</span>
                                    </td>
                                    <td>05165489418</td>
                                    <td>2022-04-22 08:50:32</td>
                                    <td className='text-primary font-weight-bold'>New</td>
                                    <td>
                                        <button type='button' className="btn btn-danger mx-1">Update</button>
                                        <button type='button' className="btn btn-danger mx-1">Status</button>
                                        <button type='button' className="btn btn-danger mx-1">Cancel</button>
                                        <button type='button' className="btn btn-danger mx-1">Request</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>458</td>
                                    <td>
                                        <span className="pl-2 "><span className='text-primary'>KGF</span>(359)</span>
                                    </td>
                                    <td>500</td>
                                    <td>
                                        <span className="pl-2">PhonePe</span>
                                    </td>
                                    <td>05165489418</td>
                                    <td>2022-04-22 08:50:32</td>
                                    <td className='text-primary font-weight-bold'>New</td>
                                    <td>
                                        <button type='button' className="btn btn-danger mx-1">Update</button>
                                        <button type='button' className="btn btn-danger mx-1">Status</button>
                                        <button type='button' className="btn btn-danger mx-1">Cancel</button>
                                        <button type='button' className="btn btn-danger mx-1">Request</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</>
  )
}
