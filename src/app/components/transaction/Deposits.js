import React from 'react'

export default function deposits() {
  return (
    <>
    {/* <h4 className='font-weight-bold my-3'>ALL CHALLANGES</h4> */}
    <div className="row ">
        <div className="col-12 grid-margin">
            <div className="card">
                <div className="card-body">
                    <h4 className="card-title">ALL DEPOSITS</h4>
                    <div className="table-responsive">
                        <table className="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th> ID</th>
                                    <th> User</th>
                                    <th> UPI APP</th>
                                    <th> TXN ID</th>
                                    <th> Amount </th>
                                    <th> Status </th>
                                    <th> Time </th>
                                    <th> Action </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>4587</td>
                                    <td>
                                        <span className="pl-2 ">KGF(359)</span>
                                    </td>
                                    <td>
                                        <span className="pl-2">PhonePe</span>
                                    </td>
                                    <td>T4649843165494651894</td>
                                    <td>200</td>
                                    <td className='font-weight-bold text-success'>success</td>
                                    <td>2022-04-22 08:50:32</td>
                                    <td>
                                        <button type='button' className="btn btn-success mx-1">Approve</button>
                                        <button type='button' className="btn btn-danger mx-1">Reject</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>4587</td>
                                    <td>
                                        <span className="pl-2 ">KGF(359)</span>
                                    </td>
                                    <td>
                                        <span className="pl-2">PhonePe</span>
                                    </td>
                                    <td>T4649843165494651894</td>
                                    <td>200</td>
                                    <td className='font-weight-bold text-success'>success</td>
                                    <td>2022-04-22 08:50:32</td>
                                    <td>
                                        <button type='button' className="btn btn-success mx-1">Approve</button>
                                        <button type='button' className="btn btn-danger mx-1">Reject</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>4587</td>
                                    <td>
                                        <span className="pl-2 ">KGF(359)</span>
                                    </td>
                                    <td>
                                        <span className="pl-2">PhonePe</span>
                                    </td>
                                    <td>T4649843165494651894</td>
                                    <td>200</td>
                                    <td className='font-weight-bold text-success'>success</td>
                                    <td>2022-04-22 08:50:32</td>
                                    <td>
                                        <button type='button' className="btn btn-success mx-1">Approve</button>
                                        <button type='button' className="btn btn-danger mx-1">Reject</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>4587</td>
                                    <td>
                                        <span className="pl-2 ">KGF(359)</span>
                                    </td>
                                    <td>
                                        <span className="pl-2">PhonePe</span>
                                    </td>
                                    <td>T4649843165494651894</td>
                                    <td>200</td>
                                    <td className='font-weight-bold text-success'>success</td>
                                    <td>2022-04-22 08:50:32</td>
                                    <td>
                                        <button type='button' className="btn btn-success mx-1">Approve</button>
                                        <button type='button' className="btn btn-danger mx-1">Reject</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>4587</td>
                                    <td>
                                        <span className="pl-2 ">KGF(359)</span>
                                    </td>
                                    <td>
                                        <span className="pl-2">PhonePe</span>
                                    </td>
                                    <td>T4649843165494651894</td>
                                    <td>200</td>
                                    <td className='font-weight-bold text-success'>success</td>
                                    <td>2022-04-22 08:50:32</td>
                                    <td>
                                        <button type='button' className="btn btn-success mx-1">Approve</button>
                                        <button type='button' className="btn btn-danger mx-1">Reject</button>
                                    </td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</>
  )
}
