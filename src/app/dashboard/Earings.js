import axios from 'axios'
import React, { useEffect, useState } from 'react'
import ReactPaginate from "react-paginate";


function Earings() {

    const [EARING, setEARING] = useState([])

    //pagination
    const [pageNumber, setPageNumber] = useState(0);

    const usersPerPage = 10;
    const pagesVisited = pageNumber * usersPerPage;

    const pageCount = Math.ceil(EARING.length / usersPerPage);

    const changePage = ({ selected }) => {
        setPageNumber(selected);
    };

    useEffect(() => {
        const access_token = localStorage.getItem("token")
        const headers = {
            Authorization: `Bearer ${access_token}`
        }
        axios.get(`http://64.227.186.66:4000/admin/Earning`, { headers })
            .then((res) => {
                setEARING(res.data)
            })
    })


    if (EARING === undefined) {
        return null
    }
    return (
        <>
            <div className="mt-3">
                <h3 className='d-flex flex-row'>TOTAL EARING :
                    <div className='pl-2'>
                        Rs. 10
                    </div>
                    <div className='pl-2'>
                        Clear wallet
                    </div>
                </h3>
            </div>
            <div className="row ">
                <div className="col-12 grid-margin">
                    <div className="card">
                        <div className="card-body">
                            <h4 className="card-title"></h4>
                            <div className="table-responsive">
                                <table className="table table-striped">
                                    <thead>
                                        <tr>

                                            <th>  </th>

                                            <th>  </th>
                                        </tr>
                                    </thead>
                                    {EARING.slice(pagesVisited, pagesVisited + usersPerPage).map((Earing) => (
                                        <tbody>


                                            <tr>

                                                <td>
                                                    <div className="d-flex">
                                                        <span className="pl-2">Earn From Challange {Earing.Earned_Form} on {Earing.createdAt}</span>
                                                    </div>
                                                </td>

                                                <td>
                                                    <div className="">{Earing.Ammount}</div>
                                                </td>
                                            </tr>

                                        </tbody>
                                    ))}
                                </table>
                            </div>
                            <div className="d-sm-flex text-center justify-content-between">
                                <div className="dataTables_info">Showing 6 to 10 of 10 entries</div>
                                <nav aria-label="Page navigation example">
                                    <ReactPaginate
                                        previousLabel={"Previous"}
                                        nextLabel={"Next"}
                                        pageCount={pageCount}
                                        onPageChange={changePage}
                                        containerClassName={"navigationButtons"}
                                        previousLinkClassName={"previousButton"}
                                        nextLinkClassName={"nextButton"}
                                        disabledClassName={"navigationDisabled"}
                                        activeClassName={"navigationActive"}

                                    />
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </>
    )
}

export default Earings