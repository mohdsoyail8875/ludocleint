import axios from 'axios'
import React, { useState, useEffect } from 'react'
import { BrowserRouter } from 'react-router-dom'

import App from './app/App'
import Login from './app/user-pages/Login'
import App2 from './app2.js/App2'



const RoleBase = () => {


    const [user, setUser] = useState()

    const role = async () => {
        const access_token = localStorage.getItem("token")
        const headers = {
            Authorization: `Bearer ${access_token}`
        }
        await axios.get(`http://64.227.186.66:4000/me`, { headers })
            .then((res) => {
                setUser(res.data)
                // console.log(res.data);

                // window.location.reload()

            })

    }
    useEffect(() => {
        role()
    }, [])


    if (!user) {
        return <App2 />
    }

    if (user.user_type === "Admin") {
        return (
            <>
                <App />
            </>

        )
    } else {
        // console.log('first')
        return (
            <div>
                <App2 />
            </div>
        )
    }

}

export default RoleBase